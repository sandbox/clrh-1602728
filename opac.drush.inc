<?php
/**
 * @file
 * drush command for harvesting and deleting biblios.
 */

/**
 * Implements hook_drush_help().
 */
function opac_drush_help($section) {
  switch ($section) {
    case 'drush:opac':
      return dt('drush command for OPAC module.');
    case 'drush:opac-harvest':
      return dt('Import a set of records into OPAC module.');
  }
}

/**
 * Implements hook_drush_command().
 */
function opac_drush_command() {
  $items = array();

  $items['opac-harvest'] = array(
    'callback'    => 'opac_drush_harvest',
    'description' => dt('Import a set of records into OPAC module.'),
    'aliases'     => array('harvest'),
    'options' => array(
      'servers' => 'A comma-separated list of servers for which to harvset.',
      'batch-size' => 'Number of biblios harvested in each batch process. Default value is harvest_batch_size variable or 1000',
      'from' => 'The first biblio number to harvest.',
      'to' => 'The last biblio number to harvest.',
      'print-id' => 'Print each biblio id',
    ),
  );
  $items['opac-purge'] = array(
    'callback'    => 'opac_drush_purge_nodes',
    'description' => dt('Delete biblio from the database.'),
    'options' => array(
      'servers' => 'A comma-separated list of servers for which to delete nodes.',
      'ids' => 'A comma-separated list of biblio ids to delete.',
      'confirm' => 'Confirm deletion. If not, a list of node that match condition is displayed',
    ),
  );
  $items['opac-servers-list'] = array(
    'callback'    => 'opac_servers_list',
    'description' => dt('Get the list of availables servers.'),
  );
  $items['opac-update'] = array(
    'callback'    => 'opac_update',
    'description' => dt('Update biblios.'),
    'options' => array(
      'servers' => 'A comma-separated list of servers for which to delete nodes.',
      'days' => 'Updates biblios whose date changed is oldest than number of days.',
      'limit' => 'Limit the number of biblios to be updated.',
      'order-by' => 'Order by itself. Accepted values: created, chenged.',
      'direction' => 'Direction for order-by option. Accepted values: ASC, DESC. default: ASC',
      'confirm' => 'Confirm update. If not, a list of node that match condition is displayed',
    ),
  );

  return $items;
}

/**
 * Prepare opac harvestiong.
 */
function opac_drush_harvest() {
  module_load_include('inc', 'opac', "includes/opac.connector");

  $servers = array();
  // Get command parameters.
  $servers = drush_get_option_list('servers');
  $verbose = drush_get_option('verbose');
  if (!$verbose) {
    $verbose = drush_get_option('v');
  }
  $batch_size = drush_get_option('batch-size');
  $from = drush_get_option('from');
  $to = drush_get_option('to');
  $print_id = drush_get_option('print-id');
  // Process harvesting.
  print opac_harvest($servers, $verbose, $batch_size, $from, $to, $print_id);
}

/**
 * Delete opac nodes.
 */
function opac_drush_purge_nodes() {
  $server_id_field = variable_get('opac_server_id_field', 'none');
  $record_id_field = variable_get('opac_record_id_field', 'none');
  // Get command parameters.
  $servers = drush_get_option_list('servers');
  $ids = drush_get_option_list('ids');
  $confirm = drush_get_option('confirm');

  // Build new query.on opac node type.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node', '=')
    ->propertyCondition('type', variable_get('opac_node_type', 'none'), '=');

  // If some record ids are passed in the command.
  // Record id can be found in the opac_record_id_field of the node.
  if (count($ids)) {
    $query->fieldCondition($record_id_field, 'value', $ids, 'IN');
  }

  // Node that will be deleted can be filtered by ILS server.
  // Server id of a node can be found in opac_server_id_field.
  if (count($servers)) {
    $query->fieldCondition($server_id_field, 'value', $servers, 'IN');
  }

  $entities = $query->execute();
  // If the query matched some node ids
  // we build an array this nodes (to_delete).
  $to_delete = array();
  if (isset($entities['node']) && count($entities['node'])) {
    $to_delete = array_keys($entities['node']);
  }

  // If --confirm parameters is passed in the command,
  // and there is node to delete: process db_delete.
  // Else, showing the matched node ids.
  if ($confirm && count($to_delete)) {
    db_delete('node')
      ->condition('nid', $to_delete, 'IN')
      ->execute();
  }
  else {
    if (count($to_delete)) {
      $string = implode(", ", $to_delete);
      echo "Following nodes match the condition(s):\n\n$string\n\n";
    }
    else {
      echo "No nodes match the condition(s)\n\n";
    }
  }
}

/**
 * Display available ILS servers.
 *
 * Just allows drush users to get the available
 * ILS server and they are enable or not.
 */
function opac_servers_list() {
  module_load_include('inc', 'opac', "includes/opac.db");

  $servers = opac_get_servers();

  echo "OPAC SERVERS:\n";
  foreach ($servers as $server) {
    echo $server['serv_id'] . ": ";
    echo $server['serv_enabled'] ? "[enabled]\n" : "[disabled] cannot harvest from this server\n";
  }
}

/**
 * Prepare nodes update.
 */
function opac_update() {
  // Get command parameters.
  $servers = drush_get_option_list('servers');
  $confirm = drush_get_option('confirm');
  $days = drush_get_option('days');
  $limit = drush_get_option('limit');
  $order_by = drush_get_option('order-by');
  $direction = drush_get_option('direction');

  // By default, direction of ordering is set to ASC.
  if (!$direction || $direction != 'ASC' || $direction != 'DESC') {
    $direction = 'ASC';
  }
  // Only allow to order by created or changed field.
  if ($order_by && ($order_by != 'created' || $order_by != 'changed')) {
    $order_by = '';
  }


  // Build new query.on opac node type.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node', '=')
    ->propertyCondition('type', variable_get('opac_node_type', 'none'), '=');

  // Node that will be deleted can be filtered by ILS server.
  // Server id of a node can be found in opac_server_id_field.
  if (count($servers)) {
    $server_id_field = variable_get('opac_server_id_field', 'none');
    $query->fieldCondition($server_id_field, 'value', $servers, 'IN');
  }

  // Calculate the max timestamp of nodes to update.
  if ($days) {
    $timestamp = time() - $days * 86400;
    $query->propertyCondition('changed', $timestamp, '<=');
  }

  if ($order_by) {
    $query->propertyOrderBy($order_by, $direction);
  }

  if ($limit) {
    $query->range(0, intval($limit));
  }

  $entities = $query->execute();
  // If the query matched some node ids
  // we build an array this nodes (to_delete).
  $to_delete = array();
  if (isset($entities['node']) && count($entities['node'])) {
    $to_update = array_keys($entities['node']);
  }

  // If --confirm parameters is passed in the command,
  // and there is node to delete: process db_delete.
  // Else, showing the matched node ids.
  if ($confirm && count($to_update)) {
    module_load_include('inc', 'opac', "includes/opac.connector");
    opac_process_update($to_update);
  }
  else {
    if (count($to_update)) {
      $string = implode(", ", $to_update);
      echo "Following nodes match the condition(s):\n\n$string\n\n";
      echo "Number of nodes matching the condition: " . count($to_update) . "\n";
    }
    else {
      echo "No nodes match the condition(s)\n\n";
    }
  }
}
