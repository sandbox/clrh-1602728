<?php
/**
 * @file
 * db functions for ils_connector
 */

/**
 * Get ILS servers from database.
 *
 * @param bool $enabled
 *   A boolean telling if we want enabled servers only
 *
 * @return array
 *   An array with all servers.
 */
function opac_get_servers($enabled = NULL) {
  $select = db_select('opac_servers', 'serv')
    ->fields('serv');

  if ($enabled) {
    $select->condition('serv_enabled', 1, '=');
  }
  $servers = array();
  $result = $select->execute();

  while ($server = $result->fetchAssoc()) {
    $servers[$server['serv_id']] = $server;
  }

  return $servers;
}

/**
 * Get one ILS server from database.
 *
 * @param string $serv_id
 *   A string containing the ILS server identifier.
 *
 * @return array
 *   An array with server information.
 */
function opac_get_server($serv_id) {
  $select = db_select('opac_servers', 'serv')
    ->fields('serv')
    ->condition('serv_id', $serv_id, '=');

  $result = $select->execute();
  return $result->fetchAssoc();
}

/**
 * Perform modifications on a ILS server.
 *
 * @param array $server
 *   A array containing new server values.
 */
function opac_server_mod($server) {
  $serv_id = db_update('opac_servers')
    ->fields(array(
      'serv_name' => $server['serv_name'],
      'serv_host' => $server['serv_host'],
      'serv_connector' => $server['serv_connector'],
      'first_bib_num' => $server['first_bib_num'],
      'last_bib_num' => $server['last_bib_num'],
      'serv_enabled' => $server['serv_enabled'],
    ))
    ->condition('serv_id', $server['serv_id'], '=')
    ->execute();
}

/**
 * Add a new ILS server in database.
 *
 * @param array $server
 *   A array containing server values.
 */
function opac_server_add($server) {
  $serv_id = db_insert('opac_servers')
    ->fields(array(
      'serv_id' => $server['serv_id'],
      'serv_name' => $server['serv_name'],
      'serv_host' => $server['serv_host'],
      'serv_connector' => $server['serv_connector'],
      'first_bib_num' => $server['first_bib_num'],
      'last_bib_num' => $server['last_bib_num'],
      'serv_enabled' => $server['serv_enabled'],
    ))
    ->execute();
}

/**
 * Delete an ILS server in database.
 *
 * @param string $serv_id
 *   A array containing server machine name.
 */
function opac_server_del($serv_id) {
  $num_deleted = db_delete('opac_servers')
    ->condition('serv_id', $serv_id)
    ->execute();
}

/**
 * Enable an ILS server.
 *
 * @param string $serv_id
 *   A array containing server machine name.
 */
function opac_enable_server($serv_id) {
  $serv_id = db_update('opac_servers')
    ->fields(array(
      'serv_enabled' => 1,
    ))
    ->condition('serv_id', $serv_id, '=')
    ->execute();
}

/**
 * Disable an ILS server.
 *
 * @param string $serv_id
 *   A array containing server machine name.
 */
function opac_disable_server($serv_id) {
  $serv_id = db_update('opac_servers')
    ->fields(array(
      'serv_enabled' => 0,
    ))
    ->condition('serv_id', $serv_id, '=')
    ->execute();
}

/**
 * Get mapping for a given ILS server and field.
 *
 * @param string $serv_id
 *   A array containing server machine name.
 *
 * @param string $fieldname
 *   A array containing field machine name.
 *
 * @return array
 *   mapping for field that match the conditions.
 */
function opac_get_mapping($serv_id, $fieldname) {
  $mapped_field = '';
  $mapped_field = db_select('opac_fields_mapping', 'fm')
    ->fields('fm')
    ->condition('serv_id', $serv_id, '=')
    ->condition('node_field_name', $fieldname, '=')
    ->execute()
    ->fetchAssoc();

  return $mapped_field;
}

/**
 * Get mapping for a given ILS server.
 *
 * @param string $serv_id
 *   A array containing server machine name.
 *
 * @return array
 *   mapping for the matched server.
 */
function opac_get_server_mapping($serv_id) {
  $result = db_select('opac_fields_mapping', 'fm')
    ->fields('fm')
    ->condition('serv_id', $serv_id, '=')
    ->execute();

  $mapping = array();
  while ($entry = $result->fetchAssoc()) {
    $mapping[$entry['node_field_name']] = $entry;
  }

  return $mapping;
}

/**
 * Add a mapping for an ILS server.
 *
 * @param array $mapping
 *   A array containing mapping values whose server identifier.
 */
function opac_mapping_add($mapping) {
  $serv_id = db_insert('opac_fields_mapping')
    ->fields(array(
      'serv_id' => $mapping['serv_id'],
      'node_field_name' => $mapping['node_field_name'],
      'node_field_label' => $mapping['node_field_label'],
      'mapped_with' => $mapping['mapped_with'],
      'vid' => $mapping['vid'],
      'vocabulary_machine_name' => $mapping['vocabulary_machine_name'],
      'vocabulary_name' => $mapping['vocabulary_name'],
      'term_field' => $mapping['term_field'],
      'term_field_label' => $mapping['term_field_label'],
      'nomatch_rule' => $mapping['nomatch_rule'],
    ))
    ->execute();
}

/**
 * Perform a mapping modification.
 *
 * @param array $mapping
 *   A array containing mapping values whose server identifier.
 */
function opac_mapping_mod($mapping) {
  $serv_id = db_update('opac_fields_mapping')
    ->fields(array(
      'mapped_with' => $mapping['mapped_with'],
      'vid' => $mapping['vid'],
      'vocabulary_machine_name' => $mapping['vocabulary_machine_name'],
      'vocabulary_name' => $mapping['vocabulary_name'],
      'term_field' => $mapping['term_field'],
      'term_field_label' => $mapping['term_field_label'],
      'nomatch_rule' => $mapping['nomatch_rule'],
    ))
    ->condition('serv_id', $mapping['serv_id'], '=')
    ->condition('node_field_name', $mapping['node_field_name'], '=')
    ->execute();
}

/**
 * Delete a mapping.
 *
 * @param array $mapping
 *   A array containing mapping values whose server identifier.
 */
function opac_mapping_del($mapping) {
  $num_deleted = db_delete('opac_fields_mapping')
    ->condition('serv_id', $mapping['serv_id'], '=')
    ->condition('node_field_name', $mapping['node_field_name'], '=')
    ->execute();
}
