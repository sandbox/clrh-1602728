<?php
/**
 * @file
 * file containing functions related to nodes processing.
 */

/**
 * Record transformation process.
 *
 * Create a new node or update it if existing.
 *
 * @param array $record
 *   Containing record values.
 *
 * @param array $mapping
 *   Which record fields should be mapped with which node fields.
 *
 * @param object $node
 *   An existing node for the record.
 *
 * @return object
 *   An new or updated node.
 */
function opac_transform_to_node($record, $mapping, $node = NULL) {
  // Means we got a new record. So create a new node.
  if (!$node) {
    $node = opac_create_node();
  }
  // Apply the mapping to the node. Means that
  // mapped fields are switched from record to the node.
  opac_record_to_node_fields($record, $node, $mapping);

  // We don't want an opac node to be promoted
  // to the front page.
  // May be opac module should provide a rules maker
  // to define if a node should be promoted?
  $node->promote = 0;
  return $node;
}

/**
 * Apply a mapping to a node.
 *
 * @param array $record
 *   Containing record values.
 *
 * @param object $node
 *   An existing node for the record.
 *
 * @param array $mappings
 *   Which record fields should be mapped with which node fields.
 */
function opac_record_to_node_fields($record, &$node, $mappings) {
  $node->title = $record['title'];

  // Loop through all mapped fields.
  // All values in remotefield fields will
  // be copied in nodefield.
  foreach ($mappings['mapped'] as $nodefield => $mapping) {
    $node->{$nodefield}['und'] = array();
    $remotefield = $mapping['mapped_with'];

    // A taxonomy mapping rule exists.
    // That means the node field is related to a vocabulary.
    if (isset($mapping['vocabulary_machine_name'])) {
      if (isset($record[$remotefield])) {
        foreach ($record[$remotefield] as $value) {
          // Retieves the term id depending of the field value.
          $tid = _opac_taxonomy_get_term($mapping['vid'], $mapping['term_field'], $value);
          if ($tid) {
            $node->{$nodefield}['und'][] = array('tid' => $tid);
          }
          // No term match the field value. So if nomatch_rule
          // is 'create', we add a new term: The remotefield value.
          elseif ($mapping['nomatch_rule'] == 'create') {
            $tid = _opac_taxonomy_new_term($value, $mapping['vid']);
            $node->{$nodefield}['und'][] = array('tid' => $tid);
          }
        }
      }
    }
    // No taxonomy mapping. Simply put
    // remotefield values in nodefield.
    else {
      if (isset($record[$remotefield])) {
        foreach ($record[$remotefield] as $value) {
          $node->{$nodefield}['und'][] = array('value' => $value);
        }
      }
    }
  }
}

/**
 * Create a new node.
 *
 * @return object
 *   A new node.
 */
function opac_create_node() {
  $node = new stdClass();
  $node->type = variable_get('opac_node_type', 'none');
  node_object_prepare($node);
  $node->uid = 1;
  $node->comment = 0;
  $node->language = 'und';
  return $node;
}

/**
 * Check if a node exists.
 *
 * Search node whose opac_server_id_field and
 * opac_record_id_field match serv_id and record id parameters.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @param string $record_id
 *   Record identifier.
 *
 * @return object
 *   The matching node or NULL.
 */
function opac_existing_node($serv_id, $record_id) {
  // In order to get an existing node, we have to
  // search it by checking server and record identifier.
  // This two fields being configurable, we take them
  // with opac_server_id_field and opac_record_id_field
  // Drupal variables.
  $server_id_field = variable_get('opac_server_id_field', 'none');
  $record_id_field = variable_get('opac_record_id_field', 'none');

  // Prepare an EntityFieldQuery.
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node', '=')
    ->propertyCondition('type', variable_get('opac_node_type', 'none'), '=')
    ->fieldCondition($record_id_field, 'value', $record_id, '=')
    ->fieldCondition($server_id_field, 'value', $serv_id, '=');

  // Process the query.
  $entities = $query->execute();
  if (count($entities)) {
    $nodes_id = array_keys($entities['node']);
    return node_load($nodes_id[0]);
  }
  return NULL;
}

/**
 * Get term id for a given vocabulary, node field name  and node field value.
 *
 * @param string $vid
 *   Vocabulary identifier.
 *
 * @param string $field
 *   The node field which is tied to the vocabulary.
 *
 * @param string $value
 *   Value to compare with vocabulary terms.
 *
 * @return int
 *   Term identifier or NULL.
 */
function _opac_taxonomy_get_term($vid, $field, $value) {
  $query = new EntityFieldQuery();

  if ($field == 'name') {
    $query->entityCondition('entity_type', 'taxonomy_term', '=')
      ->propertyCondition('name', $value, '=')
      ->propertyCondition('vid', $vid, '=');
  }
  else {
    $query->entityCondition('entity_type', 'taxonomy_term', '=')
      ->propertyCondition('vid', $vid, '=')
      ->fieldCondition($field, 'value', $value, '=');
  }

  $terms = $query->execute();
  if (!isset($terms['taxonomy_term'])) {
    return 0;
  }
  $tid = key($terms['taxonomy_term']);
  return $tid;
}

/**
 * Create a new term for a given vocabulary.
 *
 * @param string $name
 *   The value of term name field to create.
 *
 * @param int $vid
 *   The vocabulary identifier for which create teh term.
 *
 * @return int
 *   Term identifier newly created.
 */
function _opac_taxonomy_new_term($name, $vid) {
  $term = (object) array(
    'name' => $name,
    'vid' => $vid,
  );
  $status = taxonomy_term_save($term);
  return $term->tid;
}
