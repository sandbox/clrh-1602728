<?php
/**
 * @file
 * file containing functions related to harvesting.
 */

/**
 * Harvest records. Used with drush command opac-harvest.
 *
 * @param string $serv_ids
 *   A string containing the ILS server machine name.
 *
 * @param bool $verbose
 *   A boolean telling if we use verbose mode.
 *
 * @param int $batch_size
 *   A integer telling how many records to get for each request.
 *
 * @param int $from
 *   A integer containing the first record identifier to request.
 *
 * @param int $to
 *   A integer containing the last record identifier to request.
 *
 * @param bool $print_id
 *   A boolean telling if we print identifiers.
 */
function opac_harvest($serv_ids = array(), $verbose = FALSE, $batch_size = 0, $from = 0, $to = 0, $print_id = 0) {
  module_load_include('inc', 'opac', "includes/opac.db");

  $servers = array();
  if (count($serv_ids)) {
    // Loop through serv_ids in order to check they're
    // existing servers and they're enabled.
    foreach ($serv_ids as $serv_id) {
      $servers[$serv_id] = opac_get_server($serv_id);
      if (!is_array($servers[$serv_id])) {
        echo "[Warning] $serv_id is not a valid server. So skip it\n";
        unset($servers[$serv_id]);
      }
      elseif (!$servers[$serv_id]['serv_enabled']) {
        echo "[Warning] $serv_id is disabled. Cannot harvest from it\n";
        unset($servers[$serv_id]);
      }
    }
  }
  // If no server id is provided, so getting
  // all enabled servers.
  else {
    $servers = opac_get_servers(1);
  }

  if (!count($servers)) {
    echo "[Warning] No valid server to harvest. Did nothing\n";
  }

  if (!$batch_size) {
    $batch_size = variable_get('harvest_batch_size', 1000);
  }
  $batch_size = intval($batch_size);

  foreach ($servers as $server) {
    if ($from) {
      $server['first_bib_num'] = intval($from);
    }
    if ($to) {
      $server['last_bib_num'] = intval($to);
    }

    if ($server['first_bib_num'] > $server['last_bib_num']) {
      exit("[Error] first_bib_num is superior to last_bib_num\n");
    }
    // Making a king of pager.
    $num_to_process = $server['last_bib_num'] - $server['first_bib_num'];
    $num_to_process += 1;
    $num_children = ceil($num_to_process / $batch_size);

    // Create an instance of the connector.
    // Get mapping and fields list to request.
    $connector = opac_get_instance($server);
    $mapping = opac_build_fields_mapping($server['serv_id']);
    $wanted_fields = opac_build_fields_list($server['serv_id']);

    $current = $server['first_bib_num'];
    for ($i = 1; $i <= $num_children; $i++) {
      $ids = array();
      $max = $current + $batch_size;
      for ($u = $current; $u <= $max - 1; $u++) {
        if ($u == $server['last_bib_num']) {
          $ids[] = "$u";
          break;
        }
        else {
          $ids[] = "$u";
        }
      }
      $current += $batch_size;
      if ($verbose) {
        echo "    ** Harvesting biblio " . $ids[0] . " to " . end($ids) . " for server " . $server['serv_id'] . "\n";
      }

      // Processing biblios request.
      $bibs = $connector->getBiblios($ids, $wanted_fields);
      $count = 0;
      foreach ($bibs as $id => $bib) {
        // Processing biblios import.
        // Making them nodes.
        $nid = opac_import_record($server['serv_id'], $id, $mapping, $bib);
        if ($nid) {
          if ($print_id) {
            echo "      - reocord: $id => node id: $nid (from " . $server['serv_id'] . ")\n";
          }
          $count++;
        }
      }
      if ($verbose) {
        echo "        => $count imported/updated\n\n";
      }
    }
  }
}

/**
 * Update nodes.
 *
 * @param array $nodes
 *   An array containing node ids.
 */
function opac_process_update($nodes) {
  module_load_include('inc', 'opac', "includes/opac.db");
  // In which fields we can get server and record id of the node ?
  $server_id_field = variable_get('opac_server_id_field', 'none');
  $record_id_field = variable_get('opac_record_id_field', 'none');

  echo "Prepare to update nodes:\n";
  $to_process = array();
  foreach ($nodes as $nid) {
    $node = node_load($nid);
    $serv_id = $node->{$server_id_field}['und'][0]['value'];
    $id = $node->{$record_id_field}['und'][0]['value'];
    $to_process[$serv_id][] = $id;
  }

  foreach ($to_process as $serv_id => $ids) {
    // Get server informations.
    // Create an instance of the connector.
    // Get mapping and fields list to request.
    $server = opac_get_server($serv_id);
    $connector = opac_get_instance($server);
    $mapping = opac_build_fields_mapping($server['serv_id']);
    $wanted_fields = opac_build_fields_list($server['serv_id']);

    // Processing biblios request.
    $bibs = $connector->getBiblios($ids, $wanted_fields);
    foreach ($bibs as $id => $bib) {
      // Processing biblios import.
      // Making them nodes.
      $nid = opac_import_record($server['serv_id'], $id, $mapping, $bib);
      echo "node $nid updated\n";
    }
  }
}

/**
 * Import record as node.
 *
 * @param string $serv_id
 *   A string containing the ILS server machine name.
 *
 * @param int $record_id
 *   An integer containing record identifier.
 *
 * @param array $mapping
 *   An array containing a mapping between record fields and node fields.
 *
 * @param array $record
 *   An array containing record fields.
 */
function opac_import_record($serv_id, $record_id, $mapping, $record = array()) {
  module_load_include('inc', 'opac', "includes/opac.node");

  // An record means it has been delete from ILS server.
  // So deleting corresponding node from drupal.
  if (!$record) {
    $node = opac_existing_node($serv_id, $record_id);
    if ($node->nid) {
      node_delete($node->nid);
    }
    return;
  }
  // Get fields for storing server and record id.
  $server_id_field = variable_get('opac_server_id_field', 'none');
  $record_id_field = variable_get('opac_record_id_field', 'none');

  // Make a record a node.
  // Third argument pass an existing node or
  // NULL value for a new one.
  $node = opac_transform_to_node(
    $record,
    $mapping,
    opac_existing_node($serv_id, $record_id)
  );

  // Storing server and record id in the node.
  $node->{$server_id_field}['und'][0]['value'] = $serv_id;
  $node->{$record_id_field}['und'][0]['value'] = $record_id;

  // Save the node.
  $node = node_submit($node);
  node_save($node);
  return $node->nid;
}

/**
 * Get all availables connectors.
 *
 * @return array
 *   An array with availables connectors.
 */
function opac_availables_connectors() {
  $path = drupal_get_path('module', 'opac') . '/connectors/';
  $connectors = array();
  $dir = opendir($path);

  // We get all files in connectors directory
  // to build a connectors list.
  while ($file = readdir($dir)) {
    if ($file != '.' && $file != '..' && !is_dir($path . $file)) {
      // Connector file name must be in lowwer case
      // but corresponding class name must have its
      // first letter capitalized.
      $include = str_replace(".inc", "", $file);
      $class = drupal_ucfirst($include);
      module_load_include('inc', 'opac', "connectors/$include");
      $instance = new $class();
      // The identity method of an OPAC connector must return
      // an id that should match the class name and a name.
      $identity = $instance->identity();
      $connectors[$identity['id']] = $identity['name'];
    }
  }
  closedir($dir);
  return $connectors;
}

/**
 * Create an instance of a connector.
 *
 * @param array $server
 *   ILS server values.
 *
 * @return array
 *   An array with connector infos.
 */
function opac_get_instance($server) {
  module_load_include('inc', 'opac', "includes/opac.db");
  $class = $server['serv_connector'];
  // Connector file name must be in lowwer case
  // but corresponding class name must have its
  // first letter capitalized.
  $file = drupal_strtolower($class);
  module_load_include('inc', 'opac', "connectors/$file");
  $connector = new $class();
  // When creating an instance of a connector
  // we have to set the 'host' property with
  // the value defined in the server in order
  // to let the connector working with.
  $connector->host = $server['serv_host'];
  return $connector;
}
