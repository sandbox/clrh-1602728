<?php
/**
 * @file
 * function related to batch operations such as harvest record.
 */

/**
 * Perform records harvesting.
 *
 * This function is called when lauching an harvest
 * from drupal ui. Not from drush command.
 *
 * @param string $serv_id
 *   A string containing the ILS server machine name.
 *
 * @param array $ids
 *   An array of record identiers to harvest.
 *
 * @param array $context
 *   The context.
 */
function opac_import_batch_process($serv_id, $ids, &$context) {
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.node");
  module_load_include('inc', 'opac', "includes/opac.connector");

  // Get server informations.
  // Create an instance of the connector.
  // Get mapping and fields list to request.
  $server = opac_get_server($serv_id);
  $mapping = opac_build_fields_mapping($serv_id);
  $connector = opac_get_instance($server);
  $wanted_fields = opac_build_fields_list($serv_id);

  // Process biblios request.
  $bibs = $connector->getBiblios($ids, $wanted_fields);
  $context['results'][] = $bibs;
  foreach ($ids as $id) {
    $bib = array();
    $bib = $bibs[$id];
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
    }

    $context['sandbox']['progress']++;
    $context['message'] = t('(%id records done)', array('%id' => $id));
    // Importing biblios in drupal.
    $nid = opac_import_record($serv_id, $id, $mapping, $bib);
  }
}

/**
 * Perform http_request_count var incrementation.
 */
function _opac_update_http_requests() {
  $_SESSION['http_request_count']++;
}

/**
 * Finish the harvest process and display result.
 *
 * @param string $success
 *   A string containing a success message.
 *
 * @param array $results
 *   An array with records done.
 *
 * @param array $operations
 *   An array with containing operations executed.
 */
function opac_harvest_finished($success, $results, $operations) {
  $message = 'Done';
  drupal_set_message(check_plain($message), 'status', TRUE);
}
