
********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: opac
Author: Alex Arnaud <alex.arnaud@biblibre.com>
Drupal: 7
Maintainers:
 * Alex Arnaud (Alex Arnaud) <alex.arnaud@biblibre.com>
 * Henri-Damien Laurent (laurenthdl) <henridamien.laurent@biblibre.com>
 * Claire Hernandez (clrh) <claire.hernandez@biblibre.com>
 * Paul Poulain (paul_poulain) <paul.poulain@biblibre.com>

********************************************************************
DESCRIPTION:

This module enables libraries to integrate their catalog
into Drupal allowing importing records,
make advanced searches with faceted results, circulation task etc ... 

This module is intended to work with any ILS by using connectors.
Everyone can make its own by creating a php file.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/AlexArnaud/1570572

A demo website is available at http://drupac.biblibre.com and a full 
ducumentation is a work in progress http://drupac.biblibre.com/drupal-opac 

********************************************************************
-- REQUIREMENTS --

None.

********************************************************************
INSTALLATION:

0. First, create a new content type dedicated to the OPAC module.
   Add as many fields as you want to this content type. Note
   that you can add taxonomy fields, OPAC mapping rules allows
   you to map incoming fields with taxonomy term.

1. Get the OPAC module by cloning sources in sites/all/modules
   directory.

     git clone http://git.drupal.org/sandbox/AlexArnaud/1570572.git

2. Enable the OPAC module by navigating to:

     Administer > Site building > Modules
