<?php
/**
 * @file
 * Admin form for opac module.
 */

/**
 * Form constructor for the opac configuration form.
 *
 * This form allows to configure opac_node_type,
 * which fields use for record and server identifier,
 * and harvest batch size.
 *
 * @see opac_admin_form_validate()
 * @ingroup forms
 */
function opac_admin_form($form, &$form_state) {
  // User could choose opac_node_type ammong all
  // available content types.
  $types = node_type_get_types();

  // Already defined ?
  $content_types_default = variable_get('opac_node_type', 'none');
  // Add 'none' default choice if no content type
  // is already defined and create content types options
  // for opac_node_type form element.
  $content_types_options = array('none' => t('None'));
  foreach ($types as $node_type => $node_object) {
    $content_types_options[$node_type] = $node_object->name;
  }

  $form['opac_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#options' => $content_types_options,
    '#default_value' => $content_types_default,
    '#description' => t('Select the content type which will be used by the OPAC module. You may have to create one first.'),
    '#ajax' => array(
      'callback' => 'opac_admin_form_callback',
      'wrapper' => 'replace_fields_choice_div',
    ),
  );
  // Get all fields for the content_types_default
  // if it is defined.
  $fields_options = array('none' => 'None');
  $fields = field_info_instances('node', $content_types_default);
  foreach ($fields as $name => $field) {
    $fields_options[$name] = $field['label'];
  }

  $form['field_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div id="replace_fields_choice_div">',
    '#suffix' => '</div>',
  );

  $form['field_settings']['opac_record_id_field'] = array(
    '#type' => 'select',
    '#title' => t("Record identifier field"),
    '#description' => t("The field which will be used to store the unique record identifier"),
    // Retrieve all fields for opac_node_type value changes and then
    // the form is rebuilt during ajax processing.
    '#options' => !empty($form_state['values']['opac_node_type'])
    ? opac_get_fields_options($form_state['values']['opac_node_type']) : $fields_options,
    '#default_value' => variable_get('opac_record_id_field', 'none'),
  );
  $form['field_settings']['opac_server_id_field'] = array(
    '#type' => 'select',
    '#title' => t("Server identifier field"),
    '#description' => t("The field which will be used to store the unique server identifier"),
    // Retrieve all fields for opac_node_type value changes and then
    // the form is rebuilt during ajax processing.
    '#options' => !empty($form_state['values']['opac_node_type'])
    ? opac_get_fields_options($form_state['values']['opac_node_type']) : $fields_options,
    '#default_value' => variable_get('opac_server_id_field', 'none'),
  );

  $form['harvest'] = array(
    '#type' => 'fieldset',
    '#title' => t('Harvest settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['harvest']['harvest_batch_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Harvest Batch size'),
    '#default_value' => variable_get('harvest_batch_size', 1000),
    '#size' => 20,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for opac_admin_form().
 */
function opac_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Harvest_batch_size is the number of records requested
  // for each request. It must be a numeric value and
  // greater than zero.
  if (!is_numeric($values['harvest_batch_size'])) {
    form_set_error('opac', t('You must enter an integer for the Harvest Batch size.'));
  }
  elseif ($values['harvest_batch_size'] <= 0) {
    form_set_error('opac', t('Harvest Batch size must be positive.'));
  }
}

/**
 * Populate opac_record_id_field and opac_server_id_field.
 *
 * It is dropdown list of opac_admin_form.
 *
 * @param string $node_type
 *   Node type.
 *
 * @return array
 *   Fields list related to the node type.
 */
function opac_get_fields_options($node_type) {
  // Get all fields for the node_type.
  $fields = field_info_instances('node', $node_type);
  // Add 'none' default choice with others.
  $fields_options = array('none' => 'None');
  foreach ($fields as $name => $field) {
    $fields_options[$name] = $field['label'];
  }
  return $fields_options;
}

/**
 * Callback for opac_admin_form.
 */
function opac_admin_form_callback($form, $form_state) {
  return $form['field_settings'];
}

/**
 * Build overview servers page.
 *
 * @return array
 *   A renderable array.
 */
function opac_overview_servers() {
  module_load_include('inc', 'opac', "includes/opac.db");
  $base_path = drupal_get_path('module', 'opac') . '/';
  // Inclue opac.admin.css and opac.admin.js in the form.
  drupal_add_css($base_path . 'opac.admin.css');
  drupal_add_js($base_path . 'opac.admin.js');

  // This retrieve all ILS server created
  // with OPAC module.
  $servers = opac_get_servers();

  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();

  $edit_link_options['attributes']['class'][] = 'opac-edit-menu-toggle';
  foreach ($servers as $server) {
    $status = $server['serv_enabled'] ? 'disable' : 'enable';
    // Build path to enable or disable server, edit,
    // delete and access to the mapping overview page.
    $change_status = $server['serv_enabled'] ? 'admin/config/opac/server/' . $server['serv_id'] . '/disable'
      : 'admin/config/opac/server/' . $server['serv_id'] . '/enable';
    $edit = 'admin/config/opac/server/' . $server['serv_id'] . '/edit';
    $delete = 'admin/config/opac/server/' . $server['serv_id'] . '/delete';
    $mapping = 'admin/config/opac/server/' . $server['serv_id'] . '/mapping';

    $row = array(theme('opac_server_overview', array('server' => $server)));
    $row[] = array('data' => l(t($status), $change_status));
    // Add edit link as menu_contextual_links.
    // So others module (such as opac items or opac users
    // could add sub element.
    $row[] = l(t('edit'), $edit, $edit_link_options) . '<div class="opac-edit-menu collapsed">' .
      theme(
        'links',
        array(
          'links' => menu_contextual_links('opac', 'admin/config/opac/server', array($server['serv_id'])),
        )
      ) . '</div>';
    $row[] = array('data' => l(t('delete'), $delete));

    $rows[] = $row;
  }

  // Build table.
  $build['server_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No server set yet. <a href="@link">Add server</a>.', array('@link' => url('admin/config/opac/server/add'))),
  );

  return $build;
}

/**
 * Build a server view page.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @return string
 *   HTML output.
 */
function opac_server_view($serv_id) {
  module_load_include('inc', 'opac', "includes/opac.db");
  $server = opac_get_server($serv_id);
  // Just display server name for the moment.
  return "<h2>" . $server['serv_name'] . "</h2>";
}

/**
 * Form constructor for the server editing/adding form.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @see opac_admin_server_edit_form_validate()
 * @see opac_admin_server_edit_form_submit()
 * @ingroup forms
 */
function opac_admin_server_edit_form($form, &$form_state, $serv_id) {
  module_load_include('inc', 'opac', "includes/opac.connector");
  module_load_include('inc', 'opac', "includes/opac.db");
  // Are we editing an existing server or adding a new one?
  $new = $serv_id == "new" ? 1 : 0;

  $server;
  // Get server values if not adding a new one.
  if (!$new) {
    $server = opac_get_server($serv_id);
  }

  $form['new'] = array(
    '#type' => 'hidden',
    '#value' => $new ? 1 : 0,
  );
  $form['serv_name'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#title' => t('OPAC server name'),
    '#default_value' => $new ? '' : $server['serv_name'],
    '#description' => t('Enter here an arbitrary name for the server.'),
    '#required' => TRUE,
  );
  $form['serv_id'] = array(
    '#type' => 'machine_name',
    '#default_value' => $new ? '' : $serv_id,
    '#maxlength' => 32,
    '#disabled' => $new ? FALSE : TRUE,
    '#machine_name' => array(
      'exists' => 'opac_get_server',
      'source' => array('serv_name'),
    ),
  );
  $form['serv_host'] = array(
    '#type' => 'textfield',
    '#title' => t('host'),
    '#default_value' => $new ? '' : $server['serv_host'],
    '#description' => t('Url of the Server that drupac will contact. I.e: http://my-library.org'),
    '#required' => TRUE,
  );
  $form['serv_connector'] = array(
    '#type' => 'select',
    '#title' => t('ILS Connector'),
    '#default_value' => $new ? 'none' : $server['serv_connector'],
    // Populate this dropdown with available connectors.
    // The form is rebuilt during ajax processing.
    '#options' => opac_availables_connectors() + array('none' => 'None'),
    '#description' => t('Choose a connector'),
    '#required' => TRUE,
  );
  $form['first_bib_num'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('First record number'),
    '#default_value' => $new ? 0 : $server['first_bib_num'],
    '#description' => t('Number of the first record in the server.'),
  );
  $form['last_bib_num'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Last record number'),
    '#default_value' => $new ? 0 : $server['last_bib_num'],
    '#description' => t('Number of the last record in the server.'),
  );
  $form['serv_enabled'] = array(
    '#type' => 'checkbox',
    '#default_value' => $new ? 0 : $server['serv_enabled'],
    '#title' => t('Enabled ?'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Form validation handler for opac_admin_server_edit_form().
 *
 * @see opac_admin_server_edit_form_submit()
 */
function opac_admin_server_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Check required parameters.
  if (!filter_var($values['serv_host'], FILTER_VALIDATE_URL)) {
    form_set_error('opac', t('Server host is not valid url. Must looks like http://catalog.yourlibrary.com'));
  }
  if (!ctype_digit($values['first_bib_num'])) {
    form_set_error('opac', t('First record number must be an integer value'));
  }
  if (!ctype_digit($values['last_bib_num'])) {
    form_set_error('opac', t('First record number must be an integer value'));
  }
}

/**
 * Form submission handler for opac_admin_server_edit_form().
 *
 * @see opac_admin_server_edit_form_validate()
 */
function opac_admin_server_edit_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");
  $values = $form_state['values'];

  // Check if we have to create or modify a the server.
  if ($values['new']) {
    opac_server_add($values);
  }
  else {
    opac_server_mod($values);
  }
  drupal_goto('/admin/config/opac/server');
}

/**
 * Form constructor which is a confirmation step when deleting a server.
 *
 * @param string $serv_id
 *   ILS server machine name to delete.
 *
 * @see opac_admin_server_delete_confirm_form_submit()
 * @ingroup forms
 */
function opac_admin_server_delete_confirm_form($form, &$form_state, $serv_id) {
  module_load_include('inc', 'opac', "includes/opac.db");
  $server = opac_get_server($serv_id);
  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['infos'] = array(
    '#markup' => "Are you sure you want delete server '" . $server['serv_name'] . "' ?",
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  $form['actions']['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
  return $form;
}

/**
 * Form submission handler for opac_admin_server_delete_confirm_form().
 */
function opac_admin_server_delete_confirm_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");
  $values = $form_state['values'];

  // If the clicked button is confirm and not cancel.
  if ($form_state['clicked_button']['#value'] == $values['confirm']) {
    opac_server_del($values['serv_id']);
  }
  drupal_goto('admin/config/opac/server');
}

/**
 * Form constructor for global mapping form.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @see opac_admin_server_mapping_form_submit()
 * @ingroup forms
 */
function opac_admin_server_mapping_form($form, &$form_state, $serv_id) {
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");
  // Check if opac_node_type variable is set.
  $opac_node_type = variable_get('opac_node_type', 'none');
  if ($opac_node_type == 'none') {
    $form['errors'] = array(
      '#markup' => t('No node type set. You can fix this by going to') . ' '
      . l(t('OPAC settings'), 'admin/config/opac/settings/') . ' ' . t('to chose a node type and required fields.'),
    );
    drupal_set_message(t('OPAC module error: None node type set.'), 'error', TRUE);
  }
  else {
    // Get a list of mapped and unmapped fields
    // for the current opac_node_type ans server.
    $mapping = opac_build_fields_mapping($serv_id);

    // Retrieves server values and create an
    // instance of the corresponding connector.
    // So we can call biblioFields method to
    // to get ILS available fields.
    $server = opac_get_server($serv_id);
    $connector = opac_get_instance($server);
    $connector_fields = $connector->biblioFields();

    $header = array(
      'node_field' => 'Node field',
      'mapped_with' => 'Mapped with',
      'vocabulary' => 'Vocabulary',
      'term_field' => 'Term field to match',
      'nomatch_rule' => 'No match rule',
      'operations' => 'Operations',
    );

    // This section is for fields that already
    // have been mapped.
    $data = array();
    foreach ($mapping['mapped'] as $name => $values) {
      // The label of the cuurent field.
      $node_label = $values['node_field_label'];
      // The machine name of the cuurent field.
      $node_name = $values['node_field_name'];
      // This provide a link to field settings page.
      $field_link = l(t("@node_label (@node_name)", array("@node_label" => $node_label, "@node_name" => $node_name)), "admin/structure/types/manage/$opac_node_type/fields/$node_name");

      // The connector_label var is the label for
      // ILS/connector field.
      $connector_label = $connector_fields[$values['mapped_with']]['label'];
      $connector_name = $values['mapped_with'];
      $connector_html = "$connector_label ($connector_name)";

      $field_vocabulary = '';
      $nomatch_rule = '';
      $term_field = '';
      // Node fields can be related to vocabulary.
      // This informations are added in 'VOCABULARY' column.
      if (isset($values['vocabulary_machine_name'])) {
        $vocabulary_machine_name = $values['vocabulary_machine_name'];
        $vocabulary_name = $values['vocabulary_name'];
        // Provide a link to edit vocabulary.
        $field_vocabulary = l(t($vocabulary_name), "admin/structure/taxonomy/$vocabulary_machine_name/edit");
        // Also provide a link to view terms list.
        $field_vocabulary .= "  -  " . l(t('view terms list'), "admin/structure/taxonomy/$vocabulary_machine_name");
        // Nomatch_rule is a mapping options telling
        // what to do when harvesting biblios and opac module
        // attempts to match field values with existing terms.
        // So we show this option in mapping overview.
        $nomatch_rule = $values['nomatch_rule'] == 'create' ? "Create new term" : "Leave empty";

        if ($values['term_field'] == 'name') {
          $term_field = 'Term name textfield';
        }
        else {
          $term_field = l($values['term_field_label'] . " (" . $values['term_field_label'] . ")",
            "admin/structure/taxonomy/type_de_document/fields/" . $values['term_field']);
        }
      }

      $data[$name]['node_field'] = $field_link;
      $data[$name]['mapped_with'] = $connector_html;
      $data[$name]['vocabulary'] = $field_vocabulary;
      $data[$name]['term_field'] = $term_field;
      $data[$name]['nomatch_rule'] = $nomatch_rule;
      $data[$name]['operations'] = $values['operations'];
    }

    $form['serv_id'] = array(
      '#type' => 'hidden',
      '#value' => $serv_id,
    );
    $form['fields_mapped'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $data,
      '#empty' => t('No fields mapped yet'),
    );

    if (count($mapping['mapped'])) {
      $form['unmap'] = array('#type' => 'submit', '#value' => t('Unmap'));
    }
    // Section for unmapped field.
    $form['fieldset_unmapped'] = array(
      '#type' => 'fieldset',
      '#title' => t('View unmapped Fields'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['fieldset_unmapped']['fields_unmapped'] = array(
      '#markup' => theme('opac_mapping_overview', array('mapping' => $mapping['unmapped'], 'serv_id' => $serv_id)),
    );
  }

  return $form;
}

/**
 * Form submission handler for opac_admin_server_mapping_form().
 */
function opac_admin_server_mapping_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");
  $values = $form_state['values'];
  // Loop through all checked fields to unmap them.
  foreach ($values['fields_mapped'] as $name => $delete) {
    if ($delete) {
      opac_mapping_del(array('serv_id' => $values['serv_id'], 'node_field_name' => $name));
    }
  }
}

/**
 * Form constructor for mapping editing/adding form.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @param string $fieldname
 *   Field name to map.
 *
 * @see opac_admin_server_mapping_edit_form_submit()
 * @ingroup forms
 */
function opac_admin_server_mapping_edit_form($form, &$form_state, $serv_id, $fieldname) {
  module_load_include('inc', 'opac', "includes/opac.connector");
  module_load_include('inc', 'opac', "includes/opac.db");
  // Get mapping information for $fieldname and $serv_id.
  $mapping = opac_build_fields_mapping($serv_id, $fieldname);
  // Are we editing a mapped or unmapped field?
  $new = count($mapping['mapped']) ? 0 : 1;

  // Retrieves server values and create an
  // instance of the corresponding connector.
  // So we can call biblioFields method to
  // to get ILS available fields.
  $server = opac_get_server($serv_id);
  $connector = opac_get_instance($server);
  $connector_fields = $connector->biblioFields();
  $fields_options = array();
  foreach ($connector_fields as $fieldid => $field) {
    $fields_options[$fieldid] = $field['label'];
  }
  // Retrives more informations about the current field.
  $field_settings = field_info_field($fieldname);

  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['new'] = array(
    '#type' => 'hidden',
    '#value' => $new,
  );
  $form['node_field_name'] = array(
    '#type' => 'hidden',
    '#value' => $fieldname,
  );

  $node_field_label = $new ? $mapping['unmapped'][$fieldname]['label'] : $mapping['mapped'][$fieldname]['node_field_label'];
  $form['node_field_label'] = array(
    '#type' => 'hidden',
    '#value' => $node_field_label,
  );

  // Here is a message to the user on how to use this form.
  $node_field_link = l(t("$node_field_label ($fieldname)", array("@node_field_label" => $node_field_label, "@fieldname" => $fieldname)), "admin/structure/types/manage/biblio/fields/$fieldname");
  $html = "<div>Editing mapping for node field $node_field_link. ";
  $html .= "<span>Be careful of multivalued fields. Some ILS fields send back more than one value. <br />";
  $html .= "A node field than accept only one value and which is mapped with a multivalued filed will contain only the first one. <br />";
  $html .= "So if it is, you may edit this node field to make accept multiple values</span></div>";
  $form['mapping_information'] = array(
    '#markup' => $html,
  );

  $field_mapped_with = $new ? 'none' : $mapping['mapped'][$fieldname]['mapped_with'];
  $form['mapping'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mapping'),
  );
  // Build a list with all available ILS/connector fields.
  $form['mapping']['mapped_with'] = array(
    '#type' => 'select',
    '#title' => t('Mapped with'),
    '#default_value' => $field_mapped_with,
    '#options' => $fields_options + array('none' => 'Select a field'),
    '#ajax' => array(
      'callback' => 'opac_mapped_with_description_callback',
      'wrapper' => 'replace_description_field',
    ),
  );

  // Connectors should provide a description
  // for each fields it proposes.
  $form['mapping']['field_description'] = array(
    // Populate this form element with field description.
    // The form is rebuilt during ajax processing.
    '#markup' => isset($form_state['values'])
    ? opac_get_field_descrition($form_state['values']['serv_id'], $form_state['values']['mapped_with'])
    : opac_get_field_descrition($serv_id, $field_mapped_with),
    '#prefix' => '<div id="replace_description_field">',
    '#suffix' => '</div>',
  );

  // Provide additional form elements if
  // current node field to map is related
  // to a vocabuary.
  if ($field_settings['type'] == 'taxonomy_term_reference') {
    $vocabulary_machine_name = $field_settings['settings']['allowed_values'][0]['vocabulary'];
    $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);
    $vocabulary_name = $vocabulary->name;

    $vocabulary_fields = field_info_instances('taxonomy_term', $vocabulary_machine_name);
    $vocabulary_fields_opt = array('none' => 'select a term field', 'name' => 'Term name');
    foreach ($vocabulary_fields as $name => $field) {
      $vocabulary_fields_opt[$name] = $field['label'];
    }

    $form['vocabulary_machine_name'] = array(
      '#type' => 'hidden',
      '#value' => $vocabulary_machine_name,
    );
    $form['vocabulary_name'] = array(
      '#type' => 'hidden',
      '#value' => $vocabulary_name,
    );
    $form['taxonomy_rule'] = array(
      '#type' => 'fieldset',
      '#title' => check_plain(t("Taxonomy rule for vocabulary $vocabulary_name")),
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['taxonomy_rule']['term_field'] = array(
      '#type' => 'select',
      '#title' => t('Term field to compare'),
      '#default_value' => $new ? 'none' : $mapping['mapped'][$fieldname]['term_field'],
      '#options' => $vocabulary_fields_opt,
    );
    $form['taxonomy_rule']['nomatch_rule'] = array(
      '#title' => 'No match rule',
      '#type' => 'select',
      '#options' => array('none' => 'Leave empty', 'create' => 'Create new term'),
      '#default_value' => $new ? 'none' : $mapping['mapped'][$fieldname]['nomatch_rule'],
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Get a biblio field description from connector.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @param string $name
 *   field name for which get the description.
 *
 * @return string
 *   HTML output.
 */
function opac_get_field_descrition($serv_id, $name) {
  if ($name == 'none') {
    return "<div class='description'>Select a field to be mapped with current node field.</div>";
  }
  $server = opac_get_server($serv_id);
  $connector = opac_get_instance($server);
  $connector_fields = $connector->biblioFields();
  $label = $connector_fields[$name]['label'];
  $description = $connector_fields[$name]['description'];

  return "<div><strong>Description of $label field:</strong><div class='description'>$description</div>";
}

/**
 * Callback for opac_admin_server_mapping_edit_form.
 */
function opac_mapped_with_description_callback($form, &$form_state) {
  return $form['mapping']['field_description'];
}

/**
 * Form submission handler for opac_admin_server_mapping_edit_form().
 */
function opac_admin_server_mapping_edit_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");

  // The following values can be NULL be have to be set
  // in the values array. Else this causes errors in
  // mapping add or del functions.
  $values = $form_state['values'];
  if (!isset($values['term_field'])) {
    $values['term_field'] = NULL;
  }
  if (!isset($values['term_field_label'])) {
    $values['term_field_label'] = NULL;
  }
  if (!isset($values['nomatch_rule'])) {
    $values['nomatch_rule'] = NULL;
  }
  if (!isset($values['vocabulary_machine_name'])) {
    $values['vocabulary_machine_name'] = NULL;
  }
  if (!isset($values['vocabulary_name'])) {
    $values['vocabulary_name'] = NULL;
  }

  // In the case the node field is linked to a vocabulary,
  // we get the vid and the term_field_label to store it
  // with the mapping.
  // The goal is to avoid to repeat this operation for each
  // record in the harvesting process.
  $values['vid'] = NULL;
  if (isset($form_state['values']['vocabulary_machine_name'])) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($values['vocabulary_machine_name']);
    $values['vid'] = $vocabulary->vid;
    if ($values['term_field'] != 'name') {
      $vocabulary_fields = field_info_instances('taxonomy_term', $form_state['values']['vocabulary_machine_name']);
      $values['term_field_label'] = $vocabulary_fields[$values['term_field']]['label'];
    }
  }

  if ($values['new'] && $values['mapped_with']) {
    opac_mapping_add($values);
  }
  elseif (!$values['new'] && !$values['mapped_with']) {
    opac_mapping_del($values);
  }
  elseif (!$values['new']) {
    opac_mapping_mod($values);
  }
  drupal_goto("admin/config/opac/server/" . $values['serv_id'] . "/biblio-mapping");
}

/**
 * Form constructor for harvest form.
 *
 * @see opac_admin_harvest_form_submit()
 * @ingroup forms
 */
function opac_admin_harvest_form($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");

  $node_type = variable_get('opac_node_type', 'none');
  $record_id_field = variable_get('opac_record_id_field', 'none');
  $server_id_field = variable_get('opac_server_id_field', 'none');

  $errors = FALSE;
  // Unable to harvest if 'opac_node_type' variable is not set.
  if ($node_type == 'none') {
    drupal_set_message(t('Missing opac_node_type'), 'error', TRUE);
    $errors = TRUE;
  }
  // The opac_record_id_field must be set.
  if ($record_id_field == 'none') {
    drupal_set_message(t('Missing opac_record_id_field'), 'error', TRUE);
    $errors = TRUE;
  }
  // The opac_server_id_field must be set.
  if ($server_id_field == 'none') {
    drupal_set_message(t('Missing opac_server_id_field'), 'error', TRUE);
    $errors = TRUE;
  }

  // Build an additional error message telling user
  // how to fix it.
  if ($errors) {
    $form['errors'] = array(
      '#markup' => t('Missing configuration element(s). You can fix this by going to ')
      . l(t('OPAC settings'), 'admin/config/opac/settings/') . ' ' . t('to chose a node type and required fields.'),
    );
  }
  // If no error, preparing harvest form.
  else {
    // Get all enabled servers.
    $servers = opac_get_servers(1);
    // Add javascript library for the progressbar.
    drupal_add_library('system', 'ui.progressbar');

    // Fill servers array to create a checkbox for each server.
    $options = array();
    foreach ($servers as $server) {
      $options[$server['serv_id']] = $server['serv_name'];
    }

    $form['server_options'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Server(s) list'),
      '#default_value' => array(),
      '#options' => $options,
      '#description' => t('Check the ils server(s) you want harvest from.'),
    );

    $form['harvet'] = array(
      '#type' => 'submit',
      '#value' => t('Harvest'),
      '#weight' => 19,
    );
  }
  return $form;
}

/**
 * Form submission handler for opac_admin_harvest_form().
 */
function opac_admin_harvest_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.connector");

  $_SESSION['http_request_count'] = 0;
  $operations = array();
  // Loop on each server that has been checked for harvesting.
  foreach ($form_state['values']['server_options'] as $serv_id => $checked) {
    if ($checked) {
      $server = opac_get_server($serv_id);

      // The batchsize is the number of records to retrieve per request.
      $batchsize = variable_get('harvest_batch_size', 1000);
      // Calculate the total record number to harvest for the current server.
      $num_to_process = $server['last_bib_num'] - $server['first_bib_num'];
      $num_to_process += 1;
      // This determines the number of jobs to do.
      $num_children = ceil($num_to_process / $batchsize);

      // Creating jobs. Each one will be responsible
      // for request a range of record identifiers.
      // The first one will harvest from 'first_bib_num'
      // variable to 'first_bib_num' + batchsize.
      $current = $server['first_bib_num'];
      for ($i = 1; $i <= $num_children; $i++) {
        $ids = array();
        $max = $current + $batchsize;
        for ($u = $current; $u <= $max - 1; $u++) {
          if ($u == $server['last_bib_num']) {
            $ids[] = "$u";
            break;
          }
          else {
            $ids[] = "$u";
          }
        }
        // Set current to the record identifier
        // the next job will start with.
        $current += $batchsize;
        // Add the job to operations list with
        // the record ids list to request.
        $operations[] = array(
          'opac_import_batch_process',
          array($server['serv_id'], $ids),
        );
      }
    }
  }

  // Build the batch and launch the process.
  // See batch api documentation at http://drupal.org/node/180528.
  $batch = array(
    'title' => t('Processing harvest'),
    'operations' => $operations,
    'progress_message' => t('Processed @percentage %'),
    'finished' => 'opac_harvest_finished',
    'file' => drupal_get_path('module', 'opac') . '/includes/opac.batch.inc',
  );
  batch_set($batch);
}
