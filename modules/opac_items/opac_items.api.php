<?php
/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Invoked from theme_opac_items(): use id to add row or column in the table.
 *
 * @param array $build
 *   Themable table with headers and data.
 *
 * @ingroup hooks
 */
function hook_opac_items_view_alter(&$build) {
  $serverid = $build['serverid'];
  $recordid = $build['recordid'];
  $nid = $build['nid'];

  $build['header'][] = array('data' => t('Operations'), 'field' => 'operations');

  foreach ($build['rows'] as $itemid => $row) {
    $build['rows'][$itemid]['data'][] = l(t('hold'), "node/$nid/holditem/$itemid");
  }
}
