<?php
/**
 * @file
 * This file contains configuration forms for opac items module.
 */

/**
 * Form constructor for the items mapping overview form.
 *
 * @param string $serv_id
 *   ILS server machine name for which displaying mapping.
 *
 * @see opac_items_mapping_overview_form_submit()
 * @ingroup forms
 */
function opac_items_mapping_overview_form($form, $form_state, $serv_id) {
  module_load_include('inc', 'opac_items', "includes/opac_items.db");

  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['add'] = array(
    '#type' => 'link',
    '#title' => t('+ Add rule'),
    '#href' => "admin/config/opac/items/fields/add/$serv_id",
    '#weight' => -15,
  );
  $form['markup'] = array(
    '#markup' => '<br /><br />',
  );

  // Get all mapped fields to dysplay them.
  $mapping = opac_items_get_fields_mapping($serv_id);
  $form['#tree'] = TRUE;
  foreach ($mapping as $rule) {
    $key = $rule['item_field'];
    $form[$key]['item_field'] = array('#markup' => check_plain($rule['item_field']));
    $form[$key]['display_header'] = array('#markup' => check_plain($rule['display_header']));
    $form[$key]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $rule['item_field'])),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $rule['weight'],
    );
    $form[$key]['edit'] = array(
      '#type' => 'link',
      '#title' => t('Edit'),
      '#href' => "admin/config/opac/items/fields/edit/$serv_id/" . $rule['item_field'],
    );
    $form[$key]['delete'] = array(
      '#type' => 'link',
      '#title' => t('Delete'),
      '#href' => "admin/config/opac/items/fields/delete/$serv_id/" . $rule['item_field'],
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for opac_items_mapping_overview_form().
 */
function opac_items_mapping_overview_form_submit($form, $form_state) {
  module_load_include('inc', 'opac_items', "includes/opac_items.db");
  $values = $form_state['values'];
  $serv_id = $values['serv_id'];
  // Loop through mapped fields to check their new weight.
  foreach (element_children($values) as $key) {
    if (isset($values[$key]['weight'])) {
      opac_items_field_mapping_mod_weight($serv_id, $key, $values[$key]['weight']);
    }
  }
}

/**
 * Form constructor for the items mapping edit/add form.
 *
 * @param string $serv_id
 *   ILS server machine name for which displaying mapping.
 *
 * @param string $item_field
 *   Existing item field mapping.
 *
 * @see opac_items_admin_field_edit_form_validate()
 * @see opac_items_admin_field_edit_form_submit()
 * @ingroup forms
 */
function opac_items_admin_field_edit_form($form, $form_state, $serv_id, $item_field = NULL) {
  module_load_include('inc', 'opac_items', "includes/opac_items.db");
  module_load_include('inc', 'opac', "includes/opac.connector");
  module_load_include('inc', 'opac', "includes/opac.db");

  $mapping = array();
  $new = TRUE;
  // If we are editing an existing mapping, we retrieve it.
  if ($item_field) {
    $mapping = opac_items_get_field_mapping($serv_id, $item_field);
    $new = FALSE;
  }

  // Create an instance of the connector and
  // get all available fields via itemFields method.
  $server = opac_get_server($serv_id);
  $connector = opac_get_instance($server);
  $connector_fields = $connector->itemFields();
  // Build fields options for item_field dropdown list
  // with 'none' default choice.
  $fields_options = array('none' => 'Select a field');
  foreach ($connector_fields as $fieldid => $field) {
    $fields_options[$fieldid] = $field['label'];
  }
  // For a new one, remove fields that are
  // already mapped.
  if ($new) {
    $existing = opac_items_get_fields_mapping($serv_id);
    foreach ($existing as $rule) {
      unset($fields_options[$rule['item_field']]);
    }
  }

  $form['new'] = array(
    '#type' => 'hidden',
    '#value' => $new,
  );
  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['item_field'] = array(
    '#type' => 'select',
    '#title' => t('Item field'),
    '#default_value' => isset($mapping['item_field']) ? $mapping['item_field'] : 'none',
    '#options' => $fields_options,
    '#disabled' => !$new,
    '#description' => t("Item field name. This is the field the connector will return. i.e 'location'."),
    '#ajax' => array(
      'callback' => 'opac_items_field_description_callback',
      'wrapper' => 'replace_description_field',
    ),
  );
  if (!isset($mapping['item_field'])) {
    $mapping['item_field'] = 'none';
  }
  $form['field_description'] = array(
    // Populate this form element with field description.
    // The form is rebuilt during ajax processing.
    '#markup' => isset($form_state['values'])
    ? opac_items_get_field_description($serv_id, $form_state['values']['item_field'])
    : opac_items_get_field_description($serv_id, $mapping['item_field']),
    '#prefix' => '<div id="replace_description_field">',
    '#suffix' => '</div>',
  );
  $form['display_header'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#title' => t('Displayed name'),
    '#default_value' => isset($mapping['display_header']) ? $mapping['display_header'] : '',
    '#description' => t("The value which will be displayed in tbale header. i.e 'Item Location'."),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($mapping['weight']) ? $mapping['weight'] : 0,
    '#delta' => 10,
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Callback for opac_items_admin_field_edit_form.
 */
function opac_items_field_description_callback($form, &$form_state) {
  return $form['field_description'];
}

/**
 * Get field description from the connector used by the ILS server.
 *
 * @param string $serv_id
 *   ILS server machine name.
 *
 * @param string $item_field
 *   Name of the item field.
 *
 * @return string
 *   html content.
 */
function opac_items_get_field_description($serv_id, $item_field) {
  if ($item_field == 'none') {
    return "<div class='description'>Select an item field to be displayed.</div>";
  }
  // Create an instance of the connector.
  $server = opac_get_server($serv_id);
  $connector = opac_get_instance($server);
  // Call itemFields method.
  $connector_fields = $connector->itemFields();
  // Get label and description of item_field.
  $label = $connector_fields[$item_field]['label'];
  $description = $connector_fields[$item_field]['description'];

  return "<div><strong>Description of $label :</strong><div class='description'>$description</div>";

}

/**
 * Form validation handler for opac_items_admin_field_edit_form().
 *
 * @see opac_items_admin_field_edit_form_submit()
 */
function opac_items_admin_field_edit_form_validate($form, $form_state) {
  $values = $form_state['values'];
  // Check empty values.
  if ($values['item_field'] == 'none' || !$values['display_header']) {
    form_set_error('items_admin_field_edit_form', 'Some required fields are missing');
  }
}

/**
 * Form submission handler for opac_items_admin_field_edit_form().
 *
 * @see opac_items_admin_field_edit_form_validate()
 */
function opac_items_admin_field_edit_form_submit($form, $form_state) {
  module_load_include('inc', 'opac_items', "includes/opac_items.db");
  $values = $form_state['values'];

  // Add or modify submitted mapping.
  if ($values['new']) {
    opac_items_field_mapping_add($values);
  }
  else {
    opac_items_field_mapping_mod($values);
  }
  drupal_goto('admin/config/opac/server/' . $values['serv_id'] . '/items-mapping');
}

/**
 * Form constructor which is a confirmation step when deleting item mapping.
 *
 * @param string $serv_id
 *   ILS server machine name for which displaying mapping.
 *
 * @param string $item_field
 *   Existing item field mapping.
 *
 * @see opac_items_admin_field_delete_confirm_form_submit()
 * @ingroup forms
 */
function opac_items_admin_field_delete_confirm_form($form, $form_state, $serv_id, $item_field) {
  module_load_include('inc', 'opac_items', "includes/opac_items.db");
  $mapping = opac_items_get_field_mapping($serv_id, $item_field);
  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['item_field'] = array(
    '#type' => 'hidden',
    '#value' => $item_field,
  );
  $form['infos'] = array(
    '#markup' => "Are you sure you want delete this mapping: '" . $mapping['item_field'] . " => " . $mapping['display_header'] . "' ?",
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  $form['actions']['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
  return $form;
}

/**
 * Form submission handler for opac_items_admin_field_delete_confirm_form().
 */
function opac_items_admin_field_delete_confirm_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac_items', "includes/opac_items.db");
  $values = $form_state['values'];

  // Delete mapping if the clicked button equals 'confirm'.
  if ($form_state['clicked_button']['#value'] == $values['confirm']) {
    opac_items_field_mapping_del($values['serv_id'], $values['item_field']);
  }
  drupal_goto('admin/config/opac/server/' . $values['serv_id'] . '/items-mapping');
}
