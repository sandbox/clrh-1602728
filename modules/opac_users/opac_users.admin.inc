<?php
/**
 * @file
 * Confirguration forms for opac users module.
 */

/**
 * Form constructor for opac users settings form.
 *
 * @see opac_users_settings_form_submit()
 * @ingroup forms
 */
function opac_users_settings_form($form, $form_state) {
  global $user;
  $form['auth_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentification'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['auth_fieldset']['opac_users_auth_method'] = array(
    '#type' => 'radios',
    '#options' => array(
      0 => t('None, library authentification is disabled.'),
      1 => t('Use drupal account as library account.'),
      2 => t('Additional account for library.'),
    ),
    '#default_value' => variable_get('opac_users_auth_method', 0),
    '#title' => t('Which authentification method would you use?'),
  );

  // Descriptions, indexed by key.
  $descriptions = array(
    0 => t('Users could not view their holds or checkouts, hold or cancel items etc ...'),
    1 => t('Drupal login and password are the same than library login and password. This avoid users to set additional account.'),
    2 => t('Means users could set an additional login/password for the libray.'),
  );

  foreach ($form['auth_fieldset']['opac_users_auth_method']['#options'] as $key => $label) {
    $form['auth_fieldset']['opac_users_auth_method'][$key]['#description'] = $descriptions[$key];
  }

  $form['hold_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Holds/Loans'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['hold_fieldset']['opac_users_holds_allowed'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
      'item' => t('Hold items'),
      'title' => t('Hold titles'),
      'cancel' => t('Cancel holds'),
      'renew' => t('Renew loans'),
    ),
    '#default_value' => variable_get('opac_users_holds_allowed', ''),
    '#title' => t('Allow the following'),
  );

  // Descriptions, indexed by key.
  $descriptions = array(
    'item' => t('Hold a specific item of a record.'),
    'title' => t('Means hold a title and ILS is responsible for choosing the items (often the first available).'),
    'cancel' => t('Users can cancel holds from drupal opac.'),
    'renew' => t('Allows users to renew loans.'),
  );

  foreach ($form['hold_fieldset']['opac_users_holds_allowed']['#options'] as $key => $label) {
    $form['hold_fieldset']['opac_users_holds_allowed'][$key]['#description'] = $descriptions[$key];
  }

  $form['pages_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('User pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // My Holds page.
  $form['pages_fieldset']['opac_users_holds_page_allowed'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('opac_users_holds_page_allowed', 1),
    '#title' => t("Enable <em>My Holds</em> page"),
    '#description' => t('Users can view their holds list in this page.'),
  );
  $form['pages_fieldset']['opac_users_holds_page_path'] = array(
    '#type' => 'textfield',
    '#title' => t('My Holds path'),
    '#default_value' => variable_get('opac_users_holds_page_path', 'opac-user/holds'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_holds_page_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['pages_fieldset']['opac_users_holds_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('My Holds page title'),
    '#default_value' => variable_get('opac_users_holds_page_title', 'My Holds'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_holds_page_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  // My Checkouts page.
  $form['pages_fieldset']['opac_users_checkouts_page_allowed'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('opac_users_checkouts_page_allowed', 1),
    '#title' => t("Enable <em>My Checkouts</em> page"),
    '#description' => t('Users can view their checkouts list in this page.'),
  );
  $form['pages_fieldset']['opac_users_checkouts_page_path'] = array(
    '#type' => 'textfield',
    '#title' => t('My Checkouts path'),
    '#default_value' => variable_get('opac_users_checkouts_page_path', 'opac-user/checkouts'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_checkouts_page_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['pages_fieldset']['opac_users_checkouts_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('My Checkouts page title'),
    '#default_value' => variable_get('opac_users_checkouts_page_title', 'My Checkouts'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_checkouts_page_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  // Library Account page.
  $form['pages_fieldset']['opac_users_library_page_allowed'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('opac_users_library_page_allowed', 1),
    '#title' => t("Enable <em>Library Account</em> page"),
    '#description' => t('Users can view their library account in this page.'),
  );
  $form['pages_fieldset']['opac_users_library_page_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Library Account path'),
    '#default_value' => variable_get('opac_users_library_page_path', 'user/%user/opac_user'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_library_page_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['pages_fieldset']['opac_users_library_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Library Account page title'),
    '#default_value' => variable_get('opac_users_library_page_title', 'Library Account'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_library_page_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  // Library registration page.
  $form['pages_fieldset']['opac_users_library_registration_allowed'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('opac_users_library_registration_allowed', 1),
    '#title' => t("Enable <em>Library registration page</em> page"),
    '#description' => t('Enable users to register to libraries.'),
  );
  $form['pages_fieldset']['opac_users_library_registration_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Library registration path'),
    '#default_value' => variable_get('opac_users_library_registration_path', 'opac-user/register'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_library_registration_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['pages_fieldset']['opac_users_library_registration_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Library registration page title'),
    '#default_value' => variable_get('opac_users_library_registration_title', 'Library registration'),
    '#states' => array(
      'visible' => array(
        ':input[name="opac_users_library_registration_allowed"]' => array('checked' => TRUE),
      ),
    ),
  );
  return system_settings_form($form);
}

/**
 * Form submission handler for opac users settings form.
 */
function opac_users_settings_form_submit($form, $form_state) {
  drupal_set_message($form_state['values']['opac_users_auth_method']);
}

/**
 * Form constructor for the user users mapping overview form.
 *
 * @param string $serv_id
 *   ILS server identifier mappinf is for.
 *
 * @see opac_users_mapping_overview_form_submit()
 * @ingroup forms
 */
function opac_users_mapping_overview_form($form, $form_state, $serv_id) {
  module_load_include('inc', 'opac_users', "opac_users.db");

  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['add'] = array(
    '#type' => 'link',
    '#title' => t('+ Add rule'),
    '#href' => "admin/config/opac/opac_users/fields/add/$serv_id",
    '#weight' => -15,
  );
  $form['markup'] = array(
    '#markup' => '<br /><br />',
  );

  // Get mapped and unmapped fields for the current server.
  $mapping = opac_users_get_fields_mapping($serv_id);
  $form['#tree'] = TRUE;
  foreach ($mapping as $rule) {
    $key = $rule['patron_field'];
    $form[$key]['patron_field'] = array('#markup' => check_plain($rule['patron_field']));
    $form[$key]['display_header'] = array('#markup' => check_plain($rule['display_header']));
    $form[$key]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $rule['patron_field'])),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $rule['weight'],
    );
    $form[$key]['edit'] = array(
      '#type' => 'link',
      '#title' => t('Edit'),
      '#href' => "admin/config/opac/opac_users/fields/edit/$serv_id/" . $rule['patron_field'],
    );
    $form[$key]['delete'] = array(
      '#type' => 'link',
      '#title' => t('Delete'),
      '#href' => "admin/config/opac/opac_users/fields/delete/$serv_id/" . $rule['patron_field'],
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Form submission handler for opac_users_mapping_overview_form().
 */
function opac_users_mapping_overview_form_submit($form, $form_state) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  $values = $form_state['values'];
  $serv_id = $values['serv_id'];
  // Save elements weight.
  foreach (element_children($values) as $key) {
    if (isset($values[$key]['weight'])) {
      opac_users_field_mapping_mod_weight($serv_id, $key, $values[$key]['weight']);
    }
  }
}

/**
 * Form constructor for the editing/adding field form.
 *
 * @param string $serv_id
 *   ILS server identifier mappinf is for.
 *
 * @param string $patron_field
 *   The ILS patron field to map.
 *
 * @see opac_users_admin_field_edit_form_validate()
 * @see opac_users_admin_field_edit_form_submit()
 * @ingroup forms
 */
function opac_users_admin_field_edit_form($form, $form_state, $serv_id, $patron_field = NULL) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  module_load_include('inc', 'opac', "includes/opac.connector");
  module_load_include('inc', 'opac', "includes/opac.db");

  $mapping = array();
  $new = TRUE;
  // Get the mapping if we edit an existing one.
  if ($patron_field) {
    $mapping = opac_users_get_field_mapping($serv_id, $patron_field);
    $new = FALSE;
  }

  // Create an instance of the connector and
  // call patronFields method to get the available
  // fields for patron.
  $server = opac_get_server($serv_id);
  $connector = opac_get_instance($server);
  $connector_fields = $connector->patronFields();
  // Add 'none' default choice.
  $fields_options = array('none' => 'Select a field');
  // Build the options list for patron_field dropdown.
  foreach ($connector_fields as $fieldid => $field) {
    $fields_options[$fieldid] = $field['label'];
  }

  // In case creating a new mapping, all fields
  // that are already mapped are removed from the
  // choice list.
  if ($new) {
    $existing = opac_users_get_fields_mapping($serv_id);
    foreach ($existing as $rule) {
      unset($fields_options[$rule['patron_field']]);
    }
  }

  // Build the form.
  $form['new'] = array(
    '#type' => 'hidden',
    '#value' => $new,
  );
  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['patron_field'] = array(
    '#type' => 'select',
    '#title' => t('Patron field'),
    '#default_value' => isset($mapping['patron_field']) ? $mapping['patron_field'] : 'none',
    '#options' => $fields_options,
    '#disabled' => !$new,
    '#description' => t("Patron field name. This is the field the connector will return. i.e 'cardnumber'."),
    '#ajax' => array(
      'callback' => 'opac_users_mapped_with_description_callback',
      'wrapper' => 'replace_description_field',
    ),
  );
  if (!isset($mapping['patron_field'])) {
    $mapping['patron_field'] = 'none';
  }
  $form['field_description'] = array(
    // Populate this form element with field description.
    // The form is rebuilt during ajax processing.
    '#markup' => isset($form_state['values'])
    ? opac_users_get_field_descrition($serv_id, $form_state['values']['patron_field'])
    : opac_users_get_field_descrition($serv_id, $mapping['patron_field']),
    '#prefix' => '<div id="replace_description_field">',
    '#suffix' => '</div>',
  );
  $form['display_header'] = array(
    '#type' => 'textfield',
    '#size' => 25,
    '#title' => t('Displayed name'),
    '#default_value' => isset($mapping['display_header']) ? $mapping['display_header'] : '',
    '#description' => t("The value which will be displayed in tbale header. i.e 'Branch name'."),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($mapping['weight']) ? $mapping['weight'] : 0,
    '#delta' => 10,
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Callback for opac_users_admin_field_edit_form.
 */
function opac_users_mapped_with_description_callback($form, &$form_state) {
  return $form['field_description'];
}

/**
 * Get field description from the connector used by the ILS server.
 *
 * @param string $serv_id
 *   ILS server machine name.
 *
 * @param string $patron_field
 *   Name of the patron field.
 *
 * @return string
 *   html content.
 */
function opac_users_get_field_descrition($serv_id, $patron_field) {
  if ($patron_field == 'none') {
    return "<div class='description'>Select a patron field to be displayed.</div>";
  }
  // Create an instance of the 'serv_id' connector.
  $server = opac_get_server($serv_id);
  $connector = opac_get_instance($server);

  // Call patronFields method to retrieve all available fields.
  $connector_fields = $connector->patronFields();
  // Just keep label and description of current patron_field.
  $label = $connector_fields[$patron_field]['label'];
  $description = $connector_fields[$patron_field]['description'];

  return "<div><strong>Description of $label :</strong><div class='description'>$description</div>";

}

/**
 * Form validation handler for opac_users_admin_field_edit_form().
 *
 * @see opac_users_admin_field_edit_form_submit()
 */
function opac_users_admin_field_edit_form_validate($form, $form_state) {
  $values = $form_state['values'];
  // Check for an empty value in patron_field or display_header.
  if ($values['patron_field'] == 'none' || !$values['display_header']) {
    form_set_error('opac_users_admin_field_edit_form', 'Some required fields are missing');
  }
}

/**
 * Form submission handler for opac_users_admin_field_edit_form().
 *
 * @see opac_users_admin_field_edit_form_validate()
 */
function opac_users_admin_field_edit_form_submit($form, $form_state) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  $values = $form_state['values'];

  // Add or modify a mapping depending 'new' form element.
  if ($values['new']) {
    opac_users_field_mapping_add($values);
  }
  else {
    opac_users_field_mapping_mod($values);
  }
  drupal_goto('admin/config/opac/opac_users/mapping/' . $values['serv_id']);
}

/**
 * Form constructor which is a confirmation step when deleting a patron mapping.
 *
 * @param string $serv_id
 *   ILS server machine name for which displaying mapping.
 *
 * @param string $patron_field
 *   Existing patron field mapping.
 *
 * @see opac_users_admin_field_delete_confirm_form_submit()
 * @ingroup forms
 */
function opac_users_admin_field_delete_confirm_form($form, $form_state, $serv_id, $patron_field) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  // Retrieve information such as field name and header
  // of the field to be deleted.
  $mapping = opac_users_get_field_mapping($serv_id, $patron_field);
  $form['serv_id'] = array(
    '#type' => 'hidden',
    '#value' => $serv_id,
  );
  $form['patron_field'] = array(
    '#type' => 'hidden',
    '#value' => $patron_field,
  );
  $form['infos'] = array(
    '#markup' => "Are you sure you want delete this mapping: '" . $mapping['patron_field'] . " => " . $mapping['display_header'] . "' ?",
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
  $form['actions']['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
  return $form;
}

/**
 * Form submission handler for opac_users_admin_field_delete_confirm_form().
 */
function opac_users_admin_field_delete_confirm_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  $values = $form_state['values'];

  // Delete mapping if the clicked button is 'confirm'.
  if ($form_state['clicked_button']['#value'] == $values['confirm']) {
    opac_users_field_mapping_del($values['serv_id'], $values['patron_field']);
  }
  drupal_goto('admin/config/opac/opac_users/fields');
}
