<?php
/**
 * @file
 * Contains all function that build specific pages for opac users module.
 */

/**
 * Build current user checkouts page.
 *
 * @return string
 *   HTML output.
 */
function opac_users_checkouts_page() {
  module_load_include('inc', 'opac_users', "opac_users.db");
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");

  if (count($_SESSION['opac_user'])) {
    $serv_id = $_SESSION['opac_user']['opac_server'];
    $patron_id = $_SESSION['opac_user']['opac_user_id'];

    // Create an instance of the corresponding connector.
    $server = opac_get_server($serv_id);
    $connector = opac_get_instance($server);

    // Get all user's checkouts.
    $checkouts = $connector->patronCheckouts($patron_id);

    // We call for theme the page only if threre is checkouts.
    if (count($checkouts)) {
      // Format required date fields.
      $today = time();
      $dates = array();
      foreach ($checkouts as $i => $checkout) {
        $checkouts[$i]['date'] = format_date($checkout['date'], $type = 'short');
        $checkouts[$i]['duedate'] = format_date($checkout['duedate'], $type = 'short');
        // For sorting array.
        $dates[$i] = $checkout['date'];

        // Is the document overdue?
        if ($checkout['duedate'] < $today) {
          $checkouts[$i]['overdue'] = 1;
        }

        // Add server/library name.
        $checkouts[$i]['serv_name'] = $server['serv_name'];
      }

      // Sort by date..
      array_multisort($dates, SORT_DESC, $checkouts);

      return theme('opac_users_checkouts', array('checkouts' => $checkouts, 'serverid' => $serv_id));
    }
    return "<h1>" . t('You have nothing checked out.') . "</h1>";
  }
  else {
    return "<h1>" . t('You are not authentified for this library.') . "</h1>";
  }
}

/**
 * Build current user holds page.
 *
 * @return string
 *   HTML output.
 */
function opac_users_holds_page() {
  module_load_include('inc', 'opac_users', "opac_users.db");
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");

  if (count($_SESSION['opac_user'])) {
    $serv_id = $_SESSION['opac_user']['opac_server'];
    $patron_id = $_SESSION['opac_user']['opac_user_id'];

    // Get the user's server and create an instance of the connector.
    $server = opac_get_server($serv_id);
    $connector = opac_get_instance($server);

    // Get all user's holds.
    $holds = $connector->patronHolds($patron_id);

    // We call for theme the page only if threre is holds.
    if (count($holds)) {
      return theme('opac_users_holds', array('holds' => $holds, 'serverid' => $serv_id));
    }
    return "<h1>" . t('You have nothing on hold.') . "</h1>";
  }
  else {
    return "<h1>" . t('You are not authentified for this library.') . "</h1>";
  }
}

/**
 * Form constructor for the user library account form.
 *
 * This page allows users to create an opac account
 * by entering login/password and choosing the
 * ILS server they suscribed.
 *
 * @see opac_users_library_account_form_submit()
 * @ingroup forms
 */
function opac_users_library_account_form($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac_users', "opac_users.db");

  // Get only available servers.
  $servers = opac_get_servers(1);
  // Build opac_server dropdown options.
  $options = array();
  foreach ($servers as $serverid => $server) {
    $options[$serverid] = $server['serv_name'];
  }

  // Get the current user this form is for.
  global $user;
  $opac_user = opac_get_user($user->uid);

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );
  $form['opac_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Library login'),
    '#default_value' => isset($opac_user['opac_login']) ? $opac_user['opac_login'] : '',
    '#size' => 20,
    '#maxlength' => 60,
    '#required' => TRUE,
  );
  $form['opac_pass'] = array(
    '#type' => 'password',
    '#title' => t('Library password'),
    '#default_value' => isset($opac_user['opac_pass']) ? $opac_user['opac_pass'] : '',
    '#maxlength' => 128,
    '#size' => 20,
    '#required' => TRUE,
  );
  $form['opac_server'] = array(
    '#type' => 'select',
    '#title' => t('Library'),
    '#options' => $options,
    '#default_value' => isset($opac_user['opac_server']) ? $opac_user['opac_server'] : '',
    '#description' => t('Select the library for which you have an account.'),
    '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  // Test button allows to check the authentification succeeded.
  $form['actions']['test'] = array('#type' => 'submit', '#value' => t('Test'));
  return $form;
}

/**
 * Form submission handler for opac_users_library_account_form().
 */
function opac_users_library_account_form_submit($form, &$form_state) {
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");
  $values = $form_state['values'];

  // Is it an authentification test?
  $test = FALSE;
  if ($form_state['clicked_button']['#value'] == $values['test']) {
    $test = TRUE;
  }

  // Create an instance of the connector and check
  // if the authenticateUser exists.
  $server = opac_get_server($values['opac_server']);
  $connector = opac_get_instance($server);
  if (!method_exists($connector, 'authenticateUser')) {
    form_set_error('opac_users', t("Authentification error: Your Library server doesn't support authentification."));
  }

  // Processing authentification. If the returned value
  // equals 0, that means it fails.
  $patron_id = $connector->authenticateUser($values['opac_login'], $values['opac_pass']);
  if ($patron_id == 0) {
    form_set_error('opac_users', t('Authentification fails: Invalid login or password'));
  }
  // Authentification succeeded. We can create the
  // opac account if it's not a test.
  else {
    if (!$test) {
      opac_users_add($values);
      $_SESSION['opac_user'] = array(
        'opac_user_id' => $patron_id,
        'opac_server' => $values['opac_server'],
      );
    }
    drupal_set_message(t('Authentification success'), 'status', TRUE);
  }
}

/**
 * Form constructor for the user holding item form.
 *
 * It is a confirmation step to hold an item after
 * the user had clicked on hold link on the items
 * list page for a given node.
 *
 * @param object $node
 *   The opac node for which holding items.
 *
 * @param string $itemid
 *   The item identifier in ILS server.
 *
 * @see opac_users_holditem_form_submit()
 * @ingroup forms
 */
function opac_users_holditem_form($form, &$form_state, $node, $itemid = NULL) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");

  // Get which field embed server and record identifiers.
  $record_id_field = variable_get('opac_record_id_field', 'none');
  $server_id_field = variable_get('opac_server_id_field', 'none');

  // Get server and record identifiers themselves.
  $recordid = $node->{$record_id_field}['und'][0]['value'];
  $serverid = $node->{$server_id_field}['und'][0]['value'];

  $opac_user = $_SESSION['opac_user'];

  // Opac user does not exists.
  if (!isset($opac_user['opac_user_id'])) {
    $form['message'] = array(
      '#markup' => "<h4>" . t("Your are not allowed to perform this operation") . "</h4>",
    );
    $form['back'] = array('#type' => 'submit', '#value' => t('Back to items list'));
    return $form;
  }
  // This user is not a borrower of correponding ILS.
  elseif ($opac_user['opac_server'] != $serverid) {
    $form['message'] = array(
      '#markup' => "<h4>" . t("Your are not allowed to hold any items for this library") . "</h4>",
    );
    $form['back'] = array('#type' => 'submit', '#value' => t('Back to items list'));
    return $form;
  }
  $patron_id = $opac_user['opac_user_id'];

  // Hidden values of the following elements will be use
  // in the submit function only.
  $form['serverid'] = array('#type' => 'hidden', '#value' => $serverid);
  $form['recordid'] = array('#type' => 'hidden', '#value' => $recordid);
  $form['nid'] = array('#type' => 'hidden', '#value' => $node->nid);
  $form['itemid'] = array('#type' => 'hidden', '#value' => $itemid);
  $form['opacuserid'] = array('#type' => 'hidden', '#value' => $patron_id);

  // The form has been rebuilt. That means hold
  // has been confirmed. Then diplaying result message.
  if (!empty($form_state['storage']['holdresult'])) {
    $form['message'] = array(
      '#markup' => $form_state['storage']['holdresult'],
    );
  }
  // Ask user for confirm.
  else {
    $form['message'] = array(
      '#markup' => "<h4>" . t("Please confirm you want to hold") . ' ' . l($node->title, "node/$node->nid") . "</h4>",
    );
  }

  // Get specific ILS hold form.
  $server = opac_get_server($serverid);
  $connector = opac_get_instance($server);

  if (method_exists($connector, 'placeHoldForm') && empty($form_state['storage']['holdresult'])) {
    $record = array(
      'recordid' => $recordid,
      'record_title' => $node->title,
      'nid' => $node->nid,);
    $connector_form = $connector->placeHoldForm($patron_id, $record, $itemid);
    $form += $connector_form;
  }

  $form['actions'] = array('#type' => 'actions');
  if (empty($form_state['storage']['holdresult'])) {
    $form['actions']['confirm'] = array('#type' => 'submit', '#value' => t('Confirm hold'));
  }
  $form['actions']['back'] = array('#type' => 'submit', '#value' => t('Back to items list'));

  return $form;
}

/**
 * Form submission handler for opac_users_holditem_form().
 */
function opac_users_holditem_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $values = $form_state['values'];
  // Check if the user has confirmed hold or wish to go back.
  if (isset($values['confirm']) && $form_state['clicked_button']['#value'] == $values['confirm']) {
    // Place the hold.
    $server = opac_get_server($values['serverid']);
    $connector = opac_get_instance($server);
    $result = $connector->placeHold($values);

    if ($result['success']) {
      $form_state['storage']['holdresult'] = "<b>Item held with success<b><br />" . $result['message'];
    }
    else {
      $form_state['storage']['holdresult'] = "<b>Error<b><br />" . $result['message'];
    }
  }
  // Go back to the items list.
  elseif ($form_state['clicked_button']['#value'] == $values['back']) {
    drupal_goto("/node/" . $values['nid'] . "/items");
  }
}

/**
 * Form constructor for the user cancelling hold form.
 *
 * @param object $node
 *   The opac node for which holding items.
 *
 * @param string $itemid
 *   The item identifier in ILS server.
 *
 * @see opac_users_cancelhold_form_submit()
 * @ingroup forms
 */
function opac_users_cancelhold_form($form, &$form_state, $node, $itemid) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");

  // Item identifiers value will be used in the submit function only.
  $form['itemid'] = array('#type' => 'hidden', '#value' => $itemid);

  // Ask the user for confirm or not.
  $form['message'] = array(
    '#markup' => "<h4>" . t("Please confirm you want to cancel hold on") . ' ' . l($node->title, "node/$node->nid") . "</h4>",
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler for opac_users_cancelhold_form().
 */
function opac_users_cancelhold_form_submit($form, &$form_state) {
  $opac_user = $_SESSION['opac_user'];
  $values = $form_state['values'];
  $result;
  // If yes, checking if credential are correct
  // and cancel the hold.
  if (isset($opac_user['opac_user_id'])) {
    $server = opac_get_server($opac_user['opac_server']);
    $connector = opac_get_instance($server);
    $result = $connector->cancelHold($opac_user['opac_user_id'], $values['itemid']);
  }

  if ($result) {
    drupal_set_message(t('Hold canceled with success'), 'status', TRUE);
  }
  else {
    drupal_set_message(t('Unable to cancel hold'), 'error', TRUE);
  }
  drupal_goto("opac-user/holds");
}

/**
 * Form constructor for the user renewing loan form.
 *
 * @param object $node
 *   The opac node for which holding items.
 *
 * @param string $itemid
 *   The item identifier in ILS server.
 *
 * @see opac_users_renewloan_form_submit()
 * @ingroup forms
 */
function opac_users_renewloan_form($form, &$form_state, $node, $itemid) {
  module_load_include('inc', 'opac_users', "opac_users.db");
  module_load_include('inc', 'opac', "includes/opac.db");
  module_load_include('inc', 'opac', "includes/opac.connector");

  // Item identifiers value will be used in the submit function only.
  $form['itemid'] = array('#type' => 'hidden', '#value' => $itemid);

  // Ask the user for confirm or not.
  $form['message'] = array(
    '#markup' => "<h4>" . t("Please confirm you want to renew this loan:") . ' ' . l($node->title, "node/$node->nid") . "</h4>",
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Confirm'));
  return $form;
}

/**
 * Form submission handler for opac_users_renewloan_form().
 */
function opac_users_renewloan_form_submit($form, &$form_state) {
  $opac_user = $_SESSION['opac_user'];
  $values = $form_state['values'];
  $result;

  // If yes, checking if credential are correct
  // and cancel the hold.
  if (isset($opac_user['opac_user_id'])) {
    $server = opac_get_server($opac_user['opac_server']);
    $connector = opac_get_instance($server);
    $result = $connector->renewItem($opac_user['opac_user_id'], $values['itemid']);
  }

  if ($result['success']) {
    drupal_set_message(t('Renew success'), 'status', TRUE);
  }
  else {
    drupal_set_message(t('Unable to renew loan'), 'error', TRUE);
  }
  drupal_goto("opac-user/checkouts");
}

/**
 * Build user account page with ILS informations.
 *
 * This page is available for drupal users that has
 * created a valid opac account.
 *
 * @param object $account
 *   Current user account.
 *
 * @return string
 *   HTML output.
 */
function opac_users_library_account_page($account) {
  $opac_user = $_SESSION['opac_user'];
  // Check the drupal account.
  if (isset($opac_user['opac_user_id'])) {
    module_load_include('inc', 'opac_users', "opac_users.db");
    module_load_include('inc', 'opac', "includes/opac.db");
    module_load_include('inc', 'opac', "includes/opac.connector");

    // Create an instance of the connector.
    $server = opac_get_server($opac_user['opac_server']);
    $connector = opac_get_instance($server);

    // Get the mapping for the current ILS server.
    // And build a list of mapped fields to request to
    // the ILS server.
    $mapping = opac_users_get_fields_mapping($opac_user['opac_server']);
    $fields = array();
    foreach ($mapping as $values) {
      $fields[] = $values['patron_field'];
    }

    $html = '';

    // Get patron info passing the ILS identifier
    // of the user and the requested fields.
    if (method_exists($connector, 'patronInfo')) {
      $patron = $connector->patronInfo($opac_user['opac_user_id'], $fields);
      foreach ($patron as $key => $value) {
        $html .= "<div><b>$key: </b>$value</div>";
      }
      return $html;
    }
    else {
      return "<h1>" . t('Service unsupported') . "</h1>";
    }

  }
  else {
    return "<h1>" . t('Unknown user.') . "</h1>";
  }
}
