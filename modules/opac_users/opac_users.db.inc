<?php
/**
 * @file
 * db call functions for opac users module.
 */

/**
 * Get opac user informations for the current user.
 *
 * @param int $uid
 *   User id.
 *
 * @return array
 *   Opac user values.
 */
function opac_get_user($uid) {
  $select = db_select('opac_users', 'ou')
    ->fields('ou')
    ->condition('uid', $uid, '=');

  $result = $select->execute();
  return $result->fetchAssoc();
}

/**
 * Add opac user.
 *
 * @param array $opac_user
 *   Opac user values.
 */
function opac_users_add($opac_user) {
  $serv_id = db_insert('opac_users')
    ->fields(array(
      'uid' => $opac_user['uid'],
      'opac_login' => $opac_user['opac_login'],
      'opac_pass' => $opac_user['opac_pass'],
      'opac_server' => $opac_user['opac_server'],
    ))
    ->execute();
}

/**
 * Get fields mapping for a given ILS server.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @return array
 *   mapping values.
 */
function opac_users_get_fields_mapping($serv_id) {
  $select = db_select('opac_users_mapping', 'om')
    ->fields('om')
    ->condition('serv_id', $serv_id, '=')
    ->orderBy('weight', 'ASC');

  $mapping = array();
  $result = $select->execute();

  while ($row = $result->fetchAssoc()) {
    $mapping[] = $row;
  }
  return $mapping;
}

/**
 * Get field mapping for a given ILS server and field.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @param string $patron_field
 *   Field name.
 *
 * @return array
 *   mapping values.
 */
function opac_users_get_field_mapping($serv_id, $patron_field) {
  $select = db_select('opac_users_mapping', 'om')
    ->fields('om')
    ->condition('serv_id', $serv_id, '=')
    ->condition('patron_field', $patron_field, '=')
    ->orderBy('weight', 'ASC');

  $result = $select->execute();
  return $result->fetchAssoc();
}

/**
 * Add field mapping.
 *
 * @param array $mapping
 *   Mapping values.
 */
function opac_users_field_mapping_add($mapping) {
  $mid = db_insert('opac_users_mapping')
    ->fields(array(
      'serv_id' => $mapping['serv_id'],
      'patron_field' => $mapping['patron_field'],
      'display_header' => $mapping['display_header'],
      'weight' => $mapping['weight'],
    ))
    ->execute();
}

/**
 * Modify field mapping.
 *
 * @param array $mapping
 *   Mapping values.
 */
function opac_users_field_mapping_mod($mapping) {
  $mid = db_update('opac_users_mapping')
    ->fields(array(
      'display_header' => $mapping['display_header'],
      'weight' => $mapping['weight'],
    ))
    ->condition('serv_id', $mapping['serv_id'], '=')
    ->condition('patron_field', $mapping['patron_field'], '=')
    ->execute();
}

/**
 * Modify weight an existing mapped patron field.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @param string $patron_field
 *   The name of the patron field to be changed.
 *
 * @param int $weight
 *   The new weight to be applied for the patron field.
 */
function opac_users_field_mapping_mod_weight($serv_id, $patron_field, $weight) {
  $mid = db_update('opac_users_mapping')
    ->fields(array(
      'weight' => $weight,
    ))
    ->condition('serv_id', $serv_id, '=')
    ->condition('patron_field', $patron_field, '=')
    ->execute();
}

/**
 * Delete an existing mapping.
 *
 * @param string $serv_id
 *   ILS server identifier.
 *
 * @param string $patron_field
 *   The name of the patron field for which delete the mapping.
 */
function opac_users_field_mapping_del($serv_id, $patron_field) {
  $num_deleted = db_delete('opac_users_mapping')
    ->condition('serv_id', $serv_id, '=')
    ->condition('patron_field', $patron_field, '=')
    ->execute();
}
