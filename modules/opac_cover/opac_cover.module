<?php
/**
 * @file
 * Module file for the opac cover module.
 */

/**
 * Implements hook_menu().
 */
function opac_cover_menu() {
  $items['admin/config/opac/cover'] = array(
    'title' => 'Cover settings',
    'description' =>  'Configure cover settings for OPAC module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('opac_cover_admin_form'),
    'file' => 'opac_cover.admin.inc',
    'access arguments' => array('administer site configuration'),
    'weight' => 3,
  );
  return $items;
}

/**
 * Implements hook_node_load().
 */
function opac_cover_node_load($nodes, $types) {
  // What is the opac node type to work with ?
  $opac_node = variable_get('opac_node_type', 'none');
  // What is the node field containing the cover identifier?
  $field_identifier = variable_get('opac_cover_identifier', 'none');
  // In which field to put the cover url?
  $field_img = variable_get('opac_cover_field', 'none');

  if ($field_identifier == 'none' || $field_img == 'none') {
    return;
  }
  foreach ($nodes as $node) {
    // Checking if we work with the defined opac node type.
    if ($node->type == $opac_node) {
      $isbn = '';
      if ($node->{$field_identifier}) {
        $isbn = $node->{$field_identifier}['und'][0]['value'];
      }
      $node->{$field_img}['und'][0]['value'] = $isbn;
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function opac_cover_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  // In which field to put the cover url?
  $field_img = variable_get('opac_cover_field', 'none');
  $element = array();
  $item = '';
  if ($entity->{$field_img}) {
    $item = $entity->{$field_img}['und'][0]['value'];
  }

  if ($field['field_name'] == $field_img) {
    $element[0]['#markup'] = theme('opac_cover_formatter_' . $display['type'], array('item' => $item));
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function opac_cover_field_widget_info() {
  return array(
    'small_cover' => array(
      'label' => t('Small Cover'),
      'field types' => array('text'),
      'settings' => array('size' => 60),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
    'medium_cover' => array(
      'label' => t('Medium Cover'),
      'field types' => array('text'),
      'settings' => array('size' => 60),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
    'big_cover' => array(
      'label' => t('Big Cover'),
      'field types' => array('text'),
      'settings' => array('size' => 60),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function opac_cover_field_formatter_info() {
  return array(
    'smallcover' => array(
      'label' => t('Small Cover'),
      'field types' => array('text'),
    ),
    'mediumcover' => array(
      'label' => t('Medium Cover'),
      'field types' => array('text'),
    ),
    'bigcover' => array(
      'label' => t('Big Cover'),
      'field types' => array('text'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function opac_cover_theme() {
  return array(
    'opac_cover_formatter_smallcover' => array(
      'arguments' => array('element' => NULL),
    ),
    'opac_cover_formatter_mediumcover' => array(
      'arguments' => array('element' => NULL),
    ),
    'opac_cover_formatter_bigcover' => array(
      'arguments' => array('element' => NULL),
    ),
  );
}

/**
 * Returns IMG HTML tag for a small cover.
 *
 * @param array $element
 *   An associative array containing:
 *   - item: The standard identifier for which search the cover.
 *
 * @ingroup themeable
 */
function theme_opac_cover_formatter_smallcover($element) {
  $isbn = $element['item'];
  // Remove space and - from isbn.
  $isbn = preg_replace('#\s|-#', '', $isbn);
  $url = "http://images.amazon.com/images/P/" . $isbn . ".01.TZZZZZZZ.jpg";
  // Check if we got an image. If no,
  // we take nocover_small.png.
  $size = getimagesize($url);
  if (!$isbn or $size[0] == 1) {
    $url = '/' . drupal_get_path('module', 'opac_cover') . '/images/nocover_small.png';
  }
  return '<img src="' . $url . '" class="drupac-cover drupac-small-cover" />';
}

/**
 * Returns IMG HTML tag for a medium cover.
 *
 * @param array $element
 *   An associative array containing:
 *   - item: The standard identifier for which search the cover.
 *
 * @ingroup themeable
 */
function theme_opac_cover_formatter_mediumcover($element) {
  $isbn = $element['item'];
  // Remove space and - from isbn.
  $isbn = preg_replace('#\s|-#', '', $isbn);
  $url = "http://images.amazon.com/images/P/" . $isbn . ".01.jpg";
  // Check if we got an image. If no,
  // we take nocover.png.
  $size = getimagesize($url);
  if (!$isbn or $size[0] == 1) {
    $url = '/' . drupal_get_path('module', 'opac_cover') . '/images/nocover.png';
  }
  return '<img src="' . $url . '" class="drupac-cover drupac-medium-cover" />';
}

/**
 * Returns IMG HTML tag for a big cover.
 *
 * @param array $element
 *   An associative array containing:
 *   - item: The standard identifier for which search the cover.
 *
 * @ingroup themeable
 */
function theme_opac_cover_formatter_bigcover($element) {
  $isbn = $element['item'];
  // Remove space and - from isbn.
  $isbn = preg_replace('#\s|-#', '', $isbn);
  $url = "http://images.amazon.com/images/P/" . $isbn . ".jpg";
  // Check if we got an image. If no,
  // we take nocover_big.png.
  $size = getimagesize($url);
  if (!$isbn or $size[0] == 1) {
    $url = '/' . drupal_get_path('module', 'opac_cover') . '/images/nocover_big.png';
  }
  return '<img height="30%" width="30%" src="' . $url . '" class="drupac-cover drupac-medium-cover" />';
}
