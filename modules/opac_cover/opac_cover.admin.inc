<?php
/**
 * @file
 * This file contains configuration forms for opac cover module.
 */

/**
 * Form constructor for the cover module configuration page.
 *
 * @ingroup forms
 */
function opac_cover_admin_form($form, $form_state) {
  // Get all related fields for the current opac node type.
  // So users can choose among them.
  $record_fields = field_info_instances('node', variable_get('opac_node_type', 'none'));

  // Adding 'none' default value.
  $options = array('none' => t('None'));
  foreach ($record_fields as $field_name => $field) {
    $options[$field_name] = $field['label'];
  }

  // This value is the opac node field that will
  // allows to retrieve an identifier (isbn) to get
  // the cover url.
  $form['opac_cover_identifier'] = array(
    '#type' => 'select',
    '#title' => t('Field to use.'),
    '#description' => t('Select the field which cover module will use to find the cover.'),
    '#options' => $options,
    '#default_value' => variable_get('opac_cover_identifier', 'none'),
  );

  // This value is the opac node field that will
  // contains the cover url.
  $form['opac_cover_field'] = array(
    '#type' => 'select',
    '#title' => t('Field for cover.'),
    '#description' => t('Select the cover field. You may have to create one and chose a cover widget.'),
    '#options' => $options,
    '#default_value' => variable_get('opac_cover_field', 'none'),
  );
  return system_settings_form($form);
}
