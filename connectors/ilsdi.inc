<?php
/**
 * @file
 * Contains the Ilsdi class.
 *
 * ILSDI Connector is an OPAC connector for ILSDI web services
 */

class Ilsdi {

  public $host;
  protected $servicePath = '/cgi-bin/koha/ilsdi.pl';

  /**
   * Returns the identity of the connector.
   *
   * @return ARRAY
   *   - id: must be exactely the file name of the connector without extention.
   *   - name: can be whatever you want
   */
  function identity() {
    return array('id' => 'Ilsdi', 'name' => 'ILSDI Connector 2.0 for koha OPAC');
  }

  /**
   * Returns available fields for patrons.
   */
  function patronFields() {
    return array(
      'category_type' => array(
        'label' => 'Category type',
        'description' => '',
      ),
      'categorycode' => array(
        'label' => 'Category code',
        'description' => 'Patron category code',
      ),
      'email' => array(
        'label' => 'Email',
        'description' => 'Patron email',
      ),
      'borrowernumber' => array(
        'label' => 'Patron number',
        'description' => 'Patron number',
      ),
      'description' => array(
        'label' => 'Description',
        'description' => 'Description',
      ),
      'enrolmentperiod' => array(
        'label' => 'Enrolment period',
        'description' => 'Enrolment period',
      ),
      'mobile' => array(
        'label' => 'Mobile',
        'description' => 'Mobile',
      ),
      'country' => array(
        'label' => 'Country',
        'description' => 'Country',
      ),
      'dateenrolled' => array(
        'label' => 'Date enrolled',
        'description' => '',
      ),
      'dateexpiry' => array(
        'label' => 'Date expiry',
        'description' => '',
      ),
      'firstname' => array(
        'label' => 'Firstname',
        'description' => '',
      ),
      'surname' => array(
        'label' => 'Surname',
        'description' => '',
      ),
      'address' => array(
        'label' => 'Address',
        'description' => '',
      ),
      'branchname' => array(
        'label' => 'Branch name',
        'description' => '',
      ),
      'cardnumber' => array(
        'label' => 'Card number',
        'description' => '',
      ),
    );
  }

  /**
   * Returns available fields for items.
   */
  function itemFields() {
    return array(
      'holdingbranch' => array(
        'label' => 'Holding branch',
        'description' => 'The branch code that holds the item.',
      ),
      'holdingbranchname' => array(
        'label' => 'Holding branch name',
        'description' => 'The branch name that holds the item.',
      ),
      'homebranch' => array(
        'label' => 'Home branch',
        'description' => 'The branch code that own the item.',
      ),
      'homebranchname' => array(
        'label' => 'Home branch name',
        'description' => 'The branch name that own the item.',
      ),
      'wthdrawn' => array(
        'label' => 'Withdrawn ?',
        'description' => 'A boolean telling if the item has been withdrawn or not.',
      ),
      'notforloan' => array(
        'label' => 'Not for loan ?',
        'description' => 'A boolean telling if the item is not for loan.',
      ),
      'location' => array(
        'label' => 'Location',
        'description' => 'Location of the item.',
      ),
      'itemcallnumber' => array(
        'label' => 'Call number',
        'description' => 'Call number of the item.',
      ),
      'date_due' => array(
        'label' => 'Date due',
        'description' => 'The date a loaned item is due.',
      ),
      'barcode' => array(
        'label' => 'Barcode',
        'description' => 'Barcode of the item.',
      ),
      'itemlost' => array(
        'label' => 'Item lost ?',
        'description' => 'A boolean telling if the item has been lost.',
      ),
      'homebranch' => array(
        'label' => 'Home branch',
        'description' => 'A boolean telling if the item has been lost.',
      ),
      'damaged' => array(
        'label' => 'Damaged',
        'description' => 'A boolean telling if the item has been damaged.',
      ),
      'stocknumber' => array(
        'label' => 'Stock number',
        'description' => 'Stock number of the item.',
      ),
      'itype' => array(
        'label' => 'Item type',
        'description' => 'Item type.',
      ),
    );
  }

  /**
   * Returns available fields for biblios.
   */
  function biblioFields() {
    return array(
      'isbn' => array(
        'label' => 'ISBN',
        'description' => 'Return ISBN in 010$a marc field',
        'get' => '010$a',
      ),
      'language' => array(
        'label' => 'Language',
        'description' => 'Contains document language (101$a)',
        'get' => '101$a',
      ),
      'country_editor' => array(
        'label' => 'Country editor',
        'description' => 'Contains country editor (102$a)',
        'get' => '102$a',
      ),
      'pub_place' => array(
        'label' => 'Publication place',
        'description' => "Contains publication place (210\$a).This field could contain several values.<br />"
        . "So you should set the 'Number of values' option of the mapped node field to 'unlimited'.",
        'get' => '210$a',
      ),
      'editor' => array(
        'label' => 'Editor',
        'description' => "The name of the editor (210\$c).",
        'get' => '210$c',
      ),
      'pub_date' => array(
        'label' => 'Publication date',
        'description' => "Publication date (210\$d).",
        'get' => '210$d',
      ),
      'author' => array(
        'label' => 'Principal author',
        'description' => 'This field contains the principal author in 200$f marc field',
        'get' => '200$f',
      ),
      'authors' => array(
        'label' => 'Secondary authors',
        'description' => "This field contains all the authors in 700 701 701 marc field.<br />"
        . "It is multivalued, so you should set the 'Number of values' option of the mapped node field to 'unlimited'.",
        'get' => 'getAuthors',
      ),
      'subjects' => array(
        'label' => 'Subjects',
        'description' => "Related subjects of the biblio in 606\$a marc field.<br />"
        . "It is multivalued, so you should set the 'Number of values' option of the mapped node field to 'unlimited'.",
        'get' => '606$a',
      ),
      'type_doc' => array(
        'label' => 'Document type',
        'description' => "Document type in 099\$t marc field.",
        'get' => '099$t',
      ),
      'edition_information' => array(
        'label' => 'Edition informations',
        'description' => "Return informations about edition: \"place: editor, year\" (210\$a: 210\$c, 210\$d).",
        'get' => 'getEdition',
      ),
      'material_information' => array(
        'label' => 'Material informations',
        'description' => "Return material informations (215\$a: 215\$c ; 215\$d).",
        'get' => 'getMaterial',
      ),
    );
  }


  /**
   * Perform an authentification request with ILS.
   *
   * @param string $login
   *   User login.
   *
   * @param string $pass
   *   User password.
   *
   * @return bool
   *   Patron id if authentification succeeded or FALSE.
   */
  function authenticateUser($login, $pass) {
    $url = $this->host . $this->servicePath . "?service=AuthenticatePatron&username=$login&password=$pass";
    $xml = @simplexml_load_file($url);

    if (isset($xml->id)) {
      return (int) $xml->id;
    }
    else {
      return 0;
    }
  }

  /**
   * Get marc data for a given record from ILS.
   *
   * @param string $id
   *   Record ILS identifier.
   *
   * @return array
   *   an array with marc fields.
   */
  function marc($id) {
    $url = $this->host . $this->servicePath . '?service=GetRecords&id=' . $id;
    $xml = @simplexml_load_file($url);

    if ($xml->record->code == "RecordNotFound") {
      return '';
    }
    $record = @simplexml_load_string($xml->record->marcxml);
    $marc = $this->parseMarcSubfields($record);
    if (!count($marc)) {
      return '';
    }
    return $marc;
  }

  /**
   * Get marc data for a given record from ILS.
   *
   * @param array $ids
   *   List of record identifiers.
   *
   * @param array $wanted_fields
   *   Fields list we want to retrieve.
   *
   * @return array
   *   Records list that match $ids with wanted fields
   */
  function getBiblios($ids, $wanted_fields) {
    $bnums = join('+', $ids);
    $url = $this->host . $this->servicePath . '?service=GetRecords&id=' . $bnums;
    $xml = @simplexml_load_file($url);

    $available_fields = $this->biblioFields();

    $bibs = array();
    foreach ($xml->record as $record) {
      $bib = array();
      if ($record->code == "RecordNotFound") {
        continue;
      }
      $identifier = (int) $record->biblioitemnumber;

      $xml2 = @simplexml_load_string($record->marcxml);
      $bib_info_marc = $this->parseMarcSubfields($xml2);
      if (!count($bib_info_marc)) {
        continue;
      }

      // Title information.
      $bib['title'] = '';
      $title = $this->getMarcValues($bib_info_marc, '200', 'a');
      $bib['title'] = $title[0];
      if (!$bib['title']) {
        $bib['title'] = ' ';
      }
      if (drupal_strlen($bib['title']) > 255) {
        $bib['title'] = drupal_substr($bib['title'], 0, 255);
      }

      // Wanted fields.
      foreach ($wanted_fields as $field) {
        $get = $available_fields[$field]['get'];
        if (method_exists($this, $get)) {
          $value = $this->$get($bib_info_marc);
          if ($value) {
            $bib[$field] = $value;
          }
        }
        else {
          list($field_num, $subfield_num) = explode("$", $get);
          $value = $this->getMarcValues($bib_info_marc, $field_num, $subfield_num);
          if (count($value)) {
            $bib[$field] = $value;
          }
        }
        if (isset($bib[$field])) {
          foreach ($bib[$field] as $key => $val) {
            if (drupal_strlen($val) > 255) {
              $bib[$field][$key] = drupal_substr($val, 0, 255);
            }
          }
        }
      }

      unset($bib_info_marc);
      $bibs[$identifier] = $bib;
    }
    return $bibs;
  }

  /**
   * Checks information about existing items for a given record.
   *
   * This method must return 2 required fields:
   * avail: tells if the items is available or not
   * canhold: tells if the item is holdable
   *
   * @param string $id
   *   The record identifier for which we want the items informations
   *
   * @param array $wanted_fields
   *   Fields list we want to retrieve.
   *
   * @return array:
   *   Items list with wanted fields + avail and holdable fields.
   */
  function itemsStatus($id, $wanted_fields) {
    $url = $this->host . $this->servicePath . '?service=GetRecords&id=' . $id;
    $xml = @simplexml_load_file($url);

    $status = array();
    $status['holds']  = (int) count($xml->record->reserves->reserve);
    $status['order']  = (int) $xml->record->order;
    $status['avail'] = (int) count($xml->record->items->item) - count($xml->record->issues->issue);
    $status['total']  = (int) count($xml->record->items->item);

    $status['items'] = array();
    foreach ($xml->record->items->item as $item) {
      $itemnumber = (int) $item->itemnumber;
      $status['items'][$itemnumber] = array();

      foreach ($wanted_fields as $field) {
        $status['items'][$itemnumber][$field] = (string) $item->{$field};
      }

      if ($item->notforloan == 0) {
        $status['items'][$itemnumber]['canhold'] = 1;
      }

      if (isset($status['items'][$itemnumber]['avail'])) {
        $status['items'][$itemnumber]['avail']++;
      }
      else {
        $status['items'][$itemnumber]['avail'] = 1;
      }
      foreach ($xml->record->issues->issue as $issue) {
        if ((int) $item->itemnumber == (int) $issue->itemnumber) {
          $status['items'][$itemnumber]['avail']--;
        }
      }
    }
    return $status;
  }

  /**
   * Checks patron information.
   *
   * @param string $patron_id
   *   Patron identifier.
   *
   * @param array $wanted_fields
   *   Fields list we want to retrieve.
   *
   * @return array
   *   Patron information with wanted fields
   */
  function patronInfo($patron_id, $wanted_fields) {
    $url = $this->host . $this->servicePath . '?service=GetPatronInfo&patron_id=' . $patron_id;
    $xml = @simplexml_load_file($url);

    $patron = array();
    foreach ($wanted_fields as $field) {
      $patron[$field] = (string) $xml->{$field};
    }

    return $patron;
  }

  /**
   * Checks patron checkouts information.
   *
   * date and duedate Fields must be a timestamp.
   *
   * @param string $patron_id
   *   Patron identifier.
   *
   * @return array
   *   Checkouts list or FALSE.
   */
  function patronCheckouts($patron_id) {
    if (!$patron_id) {
      return FALSE;
    }

    $url = $this->host . $this->servicePath . '?service=GetPatronInfo&patron_id=' . $patron_id . '&show_loans=1';
    $xml = @simplexml_load_file($url);

    $checkouts = array();
    foreach ($xml->loans->loan as $loan) {
      // Make dates fields a timestamp.
      list($year, $month, $day) = explode('-', (string) $loan->issuedate);
      list($year_due, $month_due, $day_due) = explode('-', (string) $loan->date_due);
      $date = mktime(0, 0, 0, $month, $day, $year);
      $duedate = mktime(0, 0, 0, $month_due, $day_due, $year_due);

      $checkout['recordid'] = (int) $loan->biblionumber;
      $checkout['barcode'] = (string) $loan->barcode;
      $checkout['title'] = (string) $loan->title;
      $checkout['duedate'] = $duedate;
      $checkout['date'] = $date;
      $checkout['itemid'] = (int) $loan->itemnumber;
      $checkouts[] = $checkout;
    }
    return $checkouts;
  }

  /**
   * Checks patron holds information.
   *
   * @param string $patron_id
   *   Patron identifier.
   *
   * @return array
   *   Holds list or FALSE.
   */
  function patronHolds($patron_id) {
    if (!$patron_id) {
      return FALSE;
    }

    $url = $this->host . $this->servicePath . '?service=GetPatronInfo&patron_id=' . $patron_id . '&show_holds=1';
    $xml = @simplexml_load_file($url);

    $holds = array();
    foreach ($xml->holds->hold as $hold) {
      $hold['date'] = (string) $hold->reservedate;
      $hold['title'] = (string) $hold->title;
      $hold['recordid'] = (string) $hold->biblionumber;
      $hold['barcode'] = (string) $hold->item->barcode;
      $hold['pickuploc']  = (string) $hold->branchname;
      $hold['canceldate'] = (string) $hold->cancellationdate;
      $hold['cancelid'] = (int) $hold->biblionumber;
      $hold['status'] = "On loan";
      if ($hold->found && (string) $hold->found == "W") {
        $hold['status'] = "Waitting to be pulled";
      }
      $holds[] = $hold;
    }
    return $holds;
  }

  /**
   * Performs a loan renewal.
   *
   * @param string $patron_id
   *   Patron identifier.
   *
   * @param string $itemid
   *   Item identifier to renew.
   *
   * @return bool
   *   TRUE for success or FALSE
   */
  function renewItem($patron_id, $itemid) {
    $result = array();
    if (!$patron_id || !$itemid) {
      $result['success'] = 0;
      $result['message'] = 'Bad patron or item identifier';;
      return $result;
    }

    $server = variable_get('ils_server', 'http://opac.koha.localhost/cgi-bin/koha/ilsdi.pl');
    $url = $this->host . $this->servicePath . '?service=RenewLoan&patron_id=' . $patron_id . '&item_id=' . $itemid;
    $xml = @simplexml_load_file($url);

    if ($xml->error) {
      $result['success'] = 0;
      $result['message'] = (string) $xml->error;
    }
    $result['success'] = 1;

    return $result;
  }

  /**
   * Cancel holds for a given patron.
   *
   * @param string $patron_id
   *   Patron identifier.
   *
   * @param string $itemid
   *   Item identifier for which cancel hold.
   *
   * @return bool
   *   TRUE for success or FALSE.
   */
  function cancelHold($patron_id, $itemid) {
    if (!$patron_id || !$itemid) {
      return FALSE;
    }

    $url = $this->host . $this->servicePath . '?service=CancelHold&patron_id=' . $patron_id . '&item_id=' . $itemid;
    $xml = @simplexml_load_file($url);

    if ($xml->message != 'Canceled') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Return a form to place an hold.
   *
   * Must not provide any submit button.
   *
   * @param string $patron_id
   *   Patron identifier (required).
   *
   * @param array $record
   *   Contains recordid, record_title and nid (required).
   *
   * @param string $itemid
   *   Item identifier we want to place an hold (optional).
   *
   * @return array
   *   A drupal form.
   */
  function placeHoldForm($patron_id, $record, $itemid = NULL) {
    $form = array();

    $branch_options = array(
      'none' => t('None'),
      1 => 'BU DROIT SC. ECO',
      2 => 'BU LETTRES',
      3 => 'BU SANTE',
      4 => 'BU SCIENCES',
      13 => 'CARREFOUR DES ETUDIANTS',
      11 => 'CENTRE JURIDIQUE BRIVE',
      8 => 'ENSCI',
      7 => 'ENSIL',
      5 => 'FAC DROIT',
      6 => 'FAC LETTRES',
    );

    $form['pickup_location'] = array(
      '#type' => 'select',
      '#title' => t('Pickup location'),
      '#options' => $branch_options,
      '#default_value' => 'none',
      '#description' => t('Set this to <em>None</em> to let your library choose the pickup location.'),
    );

    $form['needed_before_date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Needed before date'),
      '#date_format' => 'd/m/Y',
      '#default_value' => '',
      '#description' => t('After which date the reservation is no longer desired'),
    );
    $form['pickup_expiry_date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Pickup expiry date'),
      '#date_format' => 'd/m/Y',
      '#default_value' => '',
      '#description' => t('After which date the document is re-shelving if not withdrawn by the member'),
    );

    return $form;
  }

  /**
   * Place hold for a given patron.
   *
   * @param array $values
   *   The form submitted values containing:
   *    - opacuserid: Patron identifier (required),
   *    - recordid: record identifier to hold (required),
   *    - itemid: item identifier to hold (optional),
   *
   * @return array
   *   success key and message key.
   */
  function placeHold($values) {
    if (!isset($values['opacuserid']) || !isset($values['recordid'])) {
      return array(
        'success' => 0,
        'message' => t('Missing elements'),
      );
    }

    $url = $this->host . $this->servicePath;
    if (isset($values['itemid']) && $values['itemid']) {
      $url .= '?service=HoldItem&patron_id=' . $values['opacuserid'] . '&bib_id=' . $values['recordid'];
      $url .= '&item_id=' . $values['itemid'];
    }
    else {
      $url .= '?service=HoldTitle&patron_id=' . $values['opacuserid'] . '&bib_id=' . $values['recordid'];
      $url .= '&request_location=127.0.0.1';
    }

    if (isset($values['pickup_location']) && $values['pickup_location'] != 'none') {
      $url .= '&pickup_location=' . $values['pickup_location'];
    }
    if (isset($values['needed_before_date'])) {
      $url .= '&needed_before_date=' . $values['needed_before_date'];
    }
    if (isset($values['pickup_expiry_date'])) {
      $url .= '&pickup_expiry_date=' . $values['pickup_expiry_date'];
    }
    $xml = @simplexml_load_file($url);
    $result = array();
    if (isset($xml->pickup_location)) {
      $pickup = (string) $xml->pickup_location;
      $result['success'] = 1;
      $result['message'] = "Items should be picked up at $pickup.";
    }
    else {
      $result['success'] = 0;
      $result['message'] = "Unable to hold this item. $url";
    }
    return $result;
  }

  /**
   * Return a registration form.
   *
   * @param object $user
   *   Drupal user object.
   *
   * @return array.
   *   a drupal form.
   */
  function patronRegistrationForm($user) {
    $form = array();

    $form['patron_identity'] = array(
      '#type' => 'fieldset',
      '#title' => t('Patron identity'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['patron_identity']['opac_surname'] = array(
      '#type' => 'textfield',
      '#title' => t('Surname'),
      '#default_value' => '',
      '#size' => 25,
      '#maxlength' => 128,
      '#required' => TRUE,
    );
    $form['patron_identity']['opac_firstname'] = array(
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#default_value' => '',
      '#size' => 25,
      '#maxlength' => 128,
      '#required' => TRUE,
    );
    $form['patron_identity']['birth_date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Date of birth'),
      '#date_format' => 'd/m/Y',
      '#default_value' => '',
    );
    $form['patron_contact'] = array(
      '#type' => 'fieldset',
      '#title' => t('Address / Contact'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['patron_contact']['opac_street_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Street number'),
      '#default_value' => '',
      '#size' => 5,
      '#required' => TRUE,
    );
    $form['patron_contact']['opac_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#default_value' => '',
      '#size' => 60,
      '#required' => TRUE,
    );
    $form['patron_contact']['opac_zip'] = array(
      '#type' => 'textfield',
      '#title' => t('Zip/Postal code'),
      '#default_value' => '',
      '#size' => 10,
      '#required' => TRUE,
    );
    $form['patron_contact']['opac_city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => '',
      '#size' => 10,
      '#required' => TRUE,
    );
    $form['patron_contact']['opac_mail'] = array(
      '#type' => 'textfield',
      '#title' => t('email'),
      '#default_value' => $user->mail,
      '#size' => 35,
      '#maxlength' => 128,
      '#required' => TRUE,
    );
    $form['opac_login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#default_value' => $user->name,
      '#size' => 20,
      '#maxlength' => 128,
      '#required' => TRUE,
    );
    $form['opac_pass'] = array(
      '#type' => 'password_confirm',
      '#size' => 25,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Retrieves patron_id for a given cardnumber.
   *
   * @param string $cardnum
   *   Patron card number.
   *
   * @return string.
   *   Patrin identifier.
   */
  function getPatronId($cardnum) {
    $server = variable_get('ils_server', 'http://opac.koha.localhost/cgi-bin/koha/ilsdi.pl');
    $url = $this->host . $this->servicePath . '?service=LookupPatron&id=' . $cardnum . '&id_type=cardnumber';
    $xml = @simplexml_load_file($url);

    return $xml->id;
  }

  /**
   * Parse marc subfields.
   *
   * @param array $marcxml
   *   Marc tags and subfields values.
   *
   * @return array
   *   An array with marc subfields
   */
  function parseMarcSubfields($marcxml) {
    $marc = array();

    // Leader.
    $marc['leader'] = (string) $marcxml->leader;

    // Control field.
    foreach ($marcxml->controlfield as $controlfield) {
      $tag = (string) $controlfield->attributes()->tag;
      $data = (string) $controlfield;
      $marc['controlfields'][$tag] = $data;
    }

    // Data field.
    $i = 0;
    foreach ($marcxml->datafield as $datafield) {
      $tag = (string) $datafield->attributes()->tag;
      $marc['datafields'][$i]['tag'] = $tag;
      $marc['datafields'][$i]['subfields'] = array();
      $y = 0;
      foreach ($datafield->subfield as $subfield) {
        $code = trim((string) $subfield->attributes()->code);
        $data = trim((string) $subfield);
        $marc['datafields'][$i]['subfields'][$y] = array('code' => $code, 'data' => $data);
        $y++;
      }
      $i++;
    }
    return $marc;
  }

  /**
   * Return values for a given tag and subfield code.
   *
   * @param array $marc
   *   Marc data of the current record.
   *
   * @param string $tag
   *   Marc tag of the wanted subfield.
   *
   * @param string $code
   *   The subfield code we want to retrieve.
   *
   * @return array
   *   an array with marc subfields values
   */
  function getMarcValues($marc, $tag, $code) {
    $values = array();
    if (!$code) {
      if (isset($marc['controlfields'][$tag])) {
        $values[] = $marc['controlfield'][$tag];
      }
    }
    else {
      foreach ($marc['datafields'] as $key => $datafield) {
        if ($datafield['tag'] == $tag) {
          foreach ($datafield['subfields'] as $key => $subfield) {
            if ($subfield['code'] == $code) {
              $values[] = $subfield['data'];
            }
          }
        }
      }
    }
    return $values;
  }

  /**
   * Return all subfields for a given field tag.
   *
   * @param array $marc
   *   Marc data of the current record.
   *
   * @param string $tag
   *   Marc tag of the wanted subfield.
   *
   * @return array
   *   Fields list with marc subfields
   */
  function getField($marc, $tag) {
    $fields = array();
    if (isset($marc['controlfields'][$tag])) {
      return $marc['controlfields'][$tag];
    }
    else {
      foreach ($marc['datafields'] as $key => $field) {
        if ($field['tag'] == $tag) {
          $fields[] = $field;
        }
      }
    }
    return $fields;
  }

  /**
   * Parse a date.
   *
   * @param string $date
   *   Date string.
   *
   * @return string
   *   A timestamp.
   */
  function dateToTimestamp($date) {
    $reg = "/([0-9].*)-([0-9].*)-([0-9].*)/";
    preg_match_all($reg, $date, $matches);
    $time = mktime(0, 0, 0, $matches[2][0], $matches[3][0], $matches[1][0]);
    return $time;
  }

  /**
   * Get "701$a 701$b", "702$a 702$b" and "712$a 712$b".
   *
   * @param array $bib_info_marc
   *   Marc data.
   *
   * @return array
   *   An array with related values.
   */
  function getAuthors($bib_info_marc) {
    $wanted = array(701, 702, 712);
    $values = array();
    foreach ($wanted as $tag) {
      $f702 = $this->getField($bib_info_marc, $tag);
      foreach ($f702 as $field) {
        $author;
        foreach ($field['subfields'] as $subfield) {
          if ($subfield['code'] == 'a') {
            $author = $subfield['data'];
            break;
          }
        }

        foreach ($field['subfields'] as $subfield) {
          if ($subfield['code'] == 'b') {
            $author .= " " . $subfield['data'];
            break;
          }
        }
        $values[] = $author;
      }
    }
    return $values;
  }

  /**
   * Get 201$a c and d from marc data.
   *
   * @param array $bib_info_marc
   *   Marc data.
   *
   * @return array
   *   An array with related values
   */
  function getEdition($bib_info_marc) {
    $place = $this->getMarcValues($bib_info_marc, '210', 'a');
    $name = $this->getMarcValues($bib_info_marc, '210', 'c');
    $date = $this->getMarcValues($bib_info_marc, '210', 'd');

    $content = $place[0] . " : " . $name[0] . ", " . $date[0];
    return array($content);
  }

  /**
   * Get 215$a c and d from marc data.
   *
   * @param array $bib_info_marc
   *   Marc data.
   *
   * @return array
   *   An array with related values.
   */
  function getMaterial($bib_info_marc) {
    $importance = $this->getMarcValues($bib_info_marc, '215', 'a');
    $other = $this->getMarcValues($bib_info_marc, '215', 'c');
    $format = $this->getMarcValues($bib_info_marc, '215', 'd');

    $content = $importance[0] . " : " . $other[0] . " ; " . $format[0];
    return array($content);
  }
}
